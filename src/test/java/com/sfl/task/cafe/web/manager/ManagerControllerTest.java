package com.sfl.task.cafe.web.manager;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.service.manager.IUserService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@ExtendWith(SpringExtension.class)
@WebMvcTest(ManagerController.class)
public class ManagerControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private IUserService userService;

    @Test
    public void viewAllPersonnel() throws Exception {
        Collection<User> userList = userFactory(2);
        when(userService.getPersonnel()).thenReturn(userList);

        mockMvc.perform(MockMvcRequestBuilders.get("/manager/user/all")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(2)))
                .andDo(print());
    }

    /**
     * Create Users for testing
     *
     * @param count how many object create
     * @return the created objects
     */
    private Collection<User> userFactory(int count) {
        List<User> users = new ArrayList<>(count);
        for (int i = 0; i < count; ++i) {
            users.add(mock(User.class));
        }
        return users;
    }
}
