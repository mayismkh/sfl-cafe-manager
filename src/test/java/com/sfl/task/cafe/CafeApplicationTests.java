package com.sfl.task.cafe;

import com.sfl.task.cafe.service.manager.IUserService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Commit;
import org.springframework.test.annotation.Rollback;

import javax.transaction.Transactional;


@SpringBootTest
@Rollback
@Transactional
public class CafeApplicationTests {

    @Autowired
    IUserService service;

    @BeforeAll
    public static void setUpBeforeClass() throws Exception {

    }

    @Test
    public void contextLoads() {
    }

    @Test
    public void UserServiceImplTest(){
        service.getAuthenticatedUser();
    }
}
