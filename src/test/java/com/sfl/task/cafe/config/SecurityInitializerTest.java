package com.sfl.task.cafe.config;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class SecurityInitializerTest {

    @BeforeAll
    public static void setUp() {
        System.out.println("\nTesting class \"SecurityInitializer\" - START.\n");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("Testing class \"SecurityInitializer\" - FINISH.\n");
    }

    @Test
    public void ConstructorTest() throws Exception {
        System.out.print("-> SecurityInitializer() - ");
        Assertions.assertNotNull(new SecurityInitializer());
        System.out.println("OK!");
    }
}
