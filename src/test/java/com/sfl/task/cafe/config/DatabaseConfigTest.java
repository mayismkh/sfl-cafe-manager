//package com.sfl.task.cafe.config;
//
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.Test;
//import org.springframework.boot.test.context.SpringBootTest;
//import org.springframework.context.annotation.ComponentScan;
//import org.springframework.test.context.ContextConfiguration;
//import org.springframework.test.context.ContextHierarchy;
//import org.springframework.test.context.web.WebAppConfiguration;
//
//import javax.persistence.EntityManagerFactory;
//
//import static org.mockito.Mockito.mock;
//
//
//@SpringBootTest
//@WebAppConfiguration
//@ContextHierarchy({
//        @ContextConfiguration(classes = RootConfig.class),
//        @ContextConfiguration(classes = WebConfig.class)
//})
//@ComponentScan(basePackages = "com.sfl.task.cafe.repository")
//public class DatabaseConfigTest {
//
//    @Test
//    public void dataSourceTest() throws Exception {
//        System.out.print("-> dataSource() - ");
//        DatabaseConfig databaseConfig = new DatabaseConfig();
//        Assertions.assertNotNull(databaseConfig.dataSource());
//        System.out.println("OK!");
//    }
//
////    @Test
////    public void jpaVendorAdapterTest() throws Exception {
////        System.out.print("-> jpaVendorAdapter() - ");
////        DatabaseConfig databaseConfig = new DatabaseConfig();
////        assertNotNull(databaseConfig.hibernateJpaVendorAdapter());
////        System.out.println("OK!");
////    }
//
////    @Test
////    public void entityManagerFactoryTest() throws Exception {
////        System.out.print("-> entityManagerFactory() - ");
////
////        DatabaseConfig databaseConfig = new DatabaseConfig();
////        DataSource dataSource = mock(DataSource.class);
////        HibernateJpaVendorAdapter jpaVendorAdapter = mock(HibernateJpaVendorAdapter.class);
////
////        assertNotNull(databaseConfig.entityManagerFactory(dataSource, jpaVendorAdapter));
////
////        System.out.println("OK!");
////    }
//
//    @Test
//    public void transactionManagerTest() {
//        System.out.print("-> transactionManager() - ");
//        DatabaseConfig databaseConfig = new DatabaseConfig();
//        EntityManagerFactory factory = mock(EntityManagerFactory.class);
//        Assertions.assertNotNull(databaseConfig.transactionManager(factory));
//        System.out.println("OK!");
//    }
//}
