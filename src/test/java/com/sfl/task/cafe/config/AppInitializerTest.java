package com.sfl.task.cafe.config;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.mock;

@SpringBootTest
public class AppInitializerTest {

    @BeforeAll
    public static void setUp() {
        System.out.println("\nTesting class \"AppInitializer\" - START.\n");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("Testing class \"AppInitializer\" - FINISH.\n");
    }

    @Test
    public void getServletConfigClassesTest() throws Exception {
        System.out.print("-> getServletConfigClasses() - ");
        AppInitializer appInitializer = new AppInitializer();
        Assertions.assertNotNull(appInitializer.getServletConfigClasses());
        System.out.println("OK!");
    }

    @Test
    public void getRootConfigClassesTest() throws Exception {
        System.out.print("-> getRootConfigClasses() - ");
        AppInitializer appInitializer = new AppInitializer();
        Assertions.assertNotNull(appInitializer.getRootConfigClasses());
        System.out.println("OK!");
    }

    @Test
    public void getServletMappingsTest() throws Exception {
        System.out.print("-> getServletMappings() - ");

        AppInitializer appInitializer = new AppInitializer();
        Assertions.assertNotNull(appInitializer.getServletMappings());

        System.out.println("OK!");
    }

//    @Test
//    public void createDispatcherServletTest() throws Exception {
//        System.out.print("-> createDispatcherServlet() - ");
//
//        AppInitializer appInitializer = new AppInitializer();
//        WebApplicationContext context = mock(WebApplicationContext.class);
//        Assertions.assertNotNull(appInitializer.createDispatcherServlet(context));
//
//        System.out.println("OK!");
//    }
}
