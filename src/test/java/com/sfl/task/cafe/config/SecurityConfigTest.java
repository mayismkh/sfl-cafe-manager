package com.sfl.task.cafe.config;

import com.sfl.task.cafe.service.manager.IUserService;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.web.WebAppConfiguration;

@SpringBootTest
@WebAppConfiguration
@ContextHierarchy({
        @ContextConfiguration(classes = RootConfig.class),
        @ContextConfiguration(classes = WebConfig.class)
})
public class SecurityConfigTest {

    @Autowired
    private IUserService userService;

    @BeforeAll
    public static void setUp() {
        System.out.println("\nTesting class \"securityConfig\" - START.\n");
    }

    @AfterAll
    public static void tearDown() {
        System.out.println("Testing class \"securityConfig\" - FINISH.\n");
    }

    @Test
    public void userServiceNotNull() throws Exception {
        System.out.print("-> userService Not Null - ");
        Assertions.assertNotNull(userService);
        System.out.println("OK!");
    }
}
