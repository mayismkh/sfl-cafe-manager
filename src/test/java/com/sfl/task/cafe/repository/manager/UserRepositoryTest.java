package com.sfl.task.cafe.repository.manager;

import com.sfl.task.cafe.domain.manager.User;
import org.junit.jupiter.api.Assertions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Collection;

@SpringBootTest
public class UserRepositoryTest {

    @Autowired
    private UserRepository userRepository;

//    @Before
//    public void setUp() throws Exception {
//
//        Collection<User> users = entityFactory(2);
//        //save user, verify has ID value after save
//        checkRepositorySaved(users);
//    }
//
//    @Test
//    public void testFetchData(){
//        /*Test data retrieval*/
//        User userA = userRepository.findByName("Bob");
//        assertNotNull(userA);
//        assertEquals(38, userA.getAge());
//        /*Get all products, list should only have two*/
//        Iterable<User> users = userRepository.findAll();
//        int count = 0;
//        for(User p : users){
//            count++;
//        }
//        assertEquals(count, 2);
//    }


//    /**
//     * Create Users for testing
//     * @param count  how many object create
//     * @return the created objects
//     */
//    private Collection<Class<E>>  entityFactory(int count){
//
//        Collection<Class<E>> entities = new ArrayList<>();
//        for (int i = 0; i < count; ++i) {
//            entities.add(mock(E.class));
//        }
//        return entities;
//    }

    /**
     * Check if repository saved data correctly
     * @param users
     */
    private void checkRepositorySaved(Collection<User> users)
    {
        for (User user: users) {
            Assertions.assertNull(user.getId());
            this.userRepository.save(user);
            Assertions.assertNull(user.getId());
        }
    }
}
