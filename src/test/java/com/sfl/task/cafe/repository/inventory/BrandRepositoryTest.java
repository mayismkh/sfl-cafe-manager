package com.sfl.task.cafe.repository.inventory;

import com.sfl.task.cafe.config.DatabaseConfig;
import com.sfl.task.cafe.config.TestConfig;
import com.sfl.task.cafe.domain.inventory.Brand;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.datasource.ConnectionHolder;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.context.junit4.SpringRunner;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@ExtendWith(SpringExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class BrandRepositoryTest {

    @Autowired
    private DataSource dataSource;

    @Autowired
    private TestEntityManager entityManager;

    @Autowired
    private BrandRepository brandRepository;

//    public ConnectionHolder getConnectionHolder() {
//        return () -> dataSource.getConnection();
//    }

    @BeforeEach
    public void setUp() throws Exception {

//        brandFactory(2);
    }

    @Test
    public void testFetchData(){
//        /*Test data retrieval*/
//        Brand brand1 = brandRepository.findByUrl("url_1");
//        Assertions.assertNotNull(brand1);
//        Assertions.assertEquals("title_1", brand1.getTitle());
//        /*Get all products, list should only have two*/
//        Iterable<Brand> brands = brandRepository.findAll();
//        int count = 0;
//        for(Brand brand : brands){
//            count++;
//        }
//        Assertions.assertEquals(count, 2);
    }

//    /**
//     * Create Brands for testing
//     * @param count  how many object create
//     */
//    private void brandFactory(int count)
//    {
//        for (int i = 0; i < count; ++i) {
//            Brand brand = new Brand();
//                brand.setTitle("title_" + i);
//                brand.setDescription("description_" + i);
//                brand.setUrl("url_" + i);
//            entityManager.persist(brand);
//        }
//    }

//    /**
//     * Check if repository saved data correctly
//     * @param brands
//     */
//    private void checkRepositorySaved(Collection<Brand> brands)
//    {
//        for (Brand brand: brands) {
//            Assertions.assertNull(brand.getId());
//            brandRepository.save(brand);
//            Assertions.assertNotNull(brand.getId());
//        }
//    }
}
