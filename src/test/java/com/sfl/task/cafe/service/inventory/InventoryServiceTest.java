package com.sfl.task.cafe.service.inventory;

import com.sfl.task.cafe.CafeManagerApplication;
import com.sfl.task.cafe.domain.inventory.Category;
import com.sfl.task.cafe.repository.inventory.BrandRepository;
import com.sfl.task.cafe.repository.inventory.CategoryRepository;
import com.sfl.task.cafe.repository.inventory.ItemRepository;
import com.sfl.task.cafe.repository.inventory.ProductRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import java.util.Optional;

import static org.mockito.Mockito.doReturn;

@SpringBootTest
        (webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = CafeManagerApplication.class,
        properties = "spring.main.allow-bean-definition-overriding=true")

@TestPropertySource(
        locations = "classpath:application.properties")
public class InventoryServiceTest {

    /**
     * Autowire in the service we want to test
     */
    @Autowired
    private IInventoryService inventoryService;

    /**
     * Create a mock implementation of the Repositories in inventory service
     */
    @MockBean
    private BrandRepository brandRepository;
    @MockBean
    private CategoryRepository categoryRepository;
    @MockBean
    private ItemRepository itemRepository;
    @MockBean
    private ProductRepository productRepository;

    @Test
    @DisplayName("Test findByUrl Success")
    public void getCategoryByUrl(){
        final String categoryUrl = "/categories/main";

        // Setup our mock repository
        Category category = new Category();
        category.setUrl(categoryUrl);
        doReturn(Optional.of(category)).when(categoryRepository).findByUrl(categoryUrl);

        // Execute the service call
        Optional<Category> returnedCategory = inventoryService.getCategoryByUrl(categoryUrl);

        // Assert the response
        Assertions.assertTrue(returnedCategory.isPresent(), "Widget was not found");
        Assertions.assertSame(returnedCategory.get(), category, "The widget returned was not the same as the mock");
    }

}
