package com.sfl.task.cafe.repository.manager;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.domain.manager.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface UserRepository  extends CrudRepository<User, Long> {
    /**
     * Return all Users
     */
    Collection<User> findAll();
    /**
     * Return User stored in DB by name
     */
    User findByFirstName(String firstName);

    /**
     * Return User from DB by user login name
     */
    User findByUsername(String username);

    /**
     * Return Users list with corresponding role
     */
    Collection<User> findAllByRole(UserRole role);

    /**
     * Delete Users with corresponding role
     */
    void deleteAllByRole(UserRole role);

    /**
     * Delete User by name
     */
    void deleteByFirstName(String name);
}
