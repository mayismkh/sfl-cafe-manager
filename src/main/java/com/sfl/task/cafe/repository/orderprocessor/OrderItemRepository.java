package com.sfl.task.cafe.repository.orderprocessor;


import com.sfl.task.cafe.domain.inventory.Item;
import com.sfl.task.cafe.domain.orderprocess.OrderItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface OrderItemRepository extends CrudRepository<OrderItem, Long> {
    /**
     * Return active Order assigned to waiter
     */
    @Query("SELECT oi FROM orderItems oi JOIN FETCH oi.order o  WHERE o.id = :orderId")
    Collection<OrderItem> findByOrderId(@Param("orderId") Long orderId);

    /**
     * Return order items by unique stock code
     */
    Optional<OrderItem> findByStockCode(String stockCode);

    /**
     * Return order items by productId
     */
    Optional<OrderItem> findByProductId(Long productId);

    /**
     * Delete order items by productId
     */
    void deleteByProductId(Long productId);

    /**
     * Delete all order items by order id
     */
    void deleteAllByOrderId(Long orderId);
}
