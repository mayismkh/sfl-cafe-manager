package com.sfl.task.cafe.repository.orderprocessor;


import com.sfl.task.cafe.domain.orderprocess.Order;
import com.sfl.task.cafe.domain.orderprocess.OrderStatus;
import com.sfl.task.cafe.domain.orderprocess.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;
import java.util.Optional;

@Repository
public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    /**
     * Return Transactions by user id
     */
    Collection<Transaction> findByUserId(Long userId);

    /**
     * Return Transaction by order id
     */
    Optional<Transaction> findByOrderId(Long orderId);
}
