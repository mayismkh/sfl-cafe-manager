package com.sfl.task.cafe.repository.manager;

import com.sfl.task.cafe.domain.manager.CafeTable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface CafeTableRepository  extends CrudRepository<CafeTable, Long> {
    /**
     * Return Table by unique number
     */
    CafeTable findByNumber(Integer number);

    /**
     * Return Table list by capacity
     */
    Collection<CafeTable> findByCapacity(Integer capacity);

    /**
     * Return Table list by waiter Id
     */
    @Query("SELECT ct FROM cafeTables ct JOIN FETCH ct.waiter w  WHERE w.id = :waiterId")
    Collection<CafeTable> findByWaiterId(@Param("waiterId") Long waiterId);

    /**
     * Delete Table from DB by number
     */
    void deleteByNumber(Integer number);
}
