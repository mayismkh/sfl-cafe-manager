package com.sfl.task.cafe.repository.inventory;

import com.sfl.task.cafe.domain.inventory.Category;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface CategoryRepository extends CrudRepository<Category, Long> {
    /**
     * Return Category by URL
     */
    Optional<Category> findByUrl(String url);

    /**
     * Delete category from DB by URL
     */
    void deleteByUrl(String url);
}
