package com.sfl.task.cafe.repository.inventory;

import com.sfl.task.cafe.domain.inventory.Product;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
    /**
     * Return Product from DB by URL
     */
    Product findByUrl(String url);

    /**
     * Return Product from DB by unique article
     */
    Product findByTitle(int article);

    /**
     * Delete Product from DB by URL
     */
    void deleteByUrl(String url);

//    /**
//     * Delete Product from DB by article
//     */
//    void deleteByArticle(int article);
//
//    /**
//     * Delete Products from DB by category unique URL
//     */
//    void deleteByCategoryUrl(String url);

//    /**
//     * Delete Products from DB by category Id
//     */
//    void deleteByCategoryId(@Param("productId")long categoryId);

    /**
     * Find Products by category Id
     */
    @Query("SELECT p FROM products p JOIN FETCH p.categories c  WHERE c.id  = :categoryId")
    Collection<Product> findByCategoryId(@Param("categoryId") long categoryId);

    /**
     * Find Products by category unique URL
     */
    Collection<Product> findByCategoriesContaining(String url);
}
