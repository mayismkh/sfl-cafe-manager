package com.sfl.task.cafe.repository.inventory;

import com.sfl.task.cafe.domain.inventory.Item;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long> {
    /**
     * Find Item by product Id
     */
    @Query("SELECT i FROM items i JOIN FETCH i.product p  WHERE p.id = :productId")
    Item findByProductId(@Param("productId") Long productId);

    /**
     * Find Item by brand Id
     */
    @Query("SELECT i FROM items i JOIN FETCH i.brand  WHERE i.brand.id = :brandId")
    Collection<Item> findByBrandId(@Param("brandId") Long brandId);
}
