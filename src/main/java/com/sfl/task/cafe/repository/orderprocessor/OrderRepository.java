package com.sfl.task.cafe.repository.orderprocessor;


import com.sfl.task.cafe.domain.orderprocess.Order;
import com.sfl.task.cafe.domain.orderprocess.OrderStatus;
import org.springframework.data.jpa.repository.Query;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public interface OrderRepository extends CrudRepository<Order, Long> {
    /**
     * Return active Order assigned to waiter
     */
    Collection<Order> findByWaiterIdAndStatusIs(Long waiterId, OrderStatus status);

    @Query("SELECT o FROM orders o WHERE o.waiterId = :waiterId AND o.tableId = :tableId ORDER BY o.updatedAt DESC")
    public Optional<Order> findFirstByWaiterIdAndTableIdAndOrderByUpdatedAtDesc(@Param("waiterId") Long waiterId, @Param("tableId") Long tableId);

    /**
     * Return Order by  waiter and table
     */
    Optional<Order> findByWaiterIdAndTableId(Long waiterId, Long tableId);

    /**
     * Return active Table by unique order code
     */
    Optional<Order> findByOrderCode(String orderCode);

    /**
     * Delete Table from DB by number
     */
    void deleteByOrderCode(String orderCode);
}
