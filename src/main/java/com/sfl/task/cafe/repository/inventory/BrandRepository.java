package com.sfl.task.cafe.repository.inventory;

import com.sfl.task.cafe.domain.inventory.Brand;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BrandRepository extends CrudRepository<Brand, Long> {
    /**
     * Return Brand by title
     */
    Brand findByTitle(String title);

    /**
     * Return Brand by URL
     */
    Brand findByUrl(String url);

    /**
     * Delete brand from DB by URL
     */
    void deleteByUrl(String url);

    /**
     * Delete brand from DB by URL
     */
    void deleteByTitle(String title);
}
