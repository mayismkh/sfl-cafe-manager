package com.sfl.task.cafe;

import com.sfl.task.cafe.messaging.manager.CafeManagerChannels;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableJpaRepositories(basePackages = { "com.sfl.task.cafe" })
@EntityScan(basePackages = { "com.sfl.task.cafe"})
//@EnableBinding(CafeManagerChannels.class)
public class CafeManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CafeManagerApplication.class, "--debug");
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
}
