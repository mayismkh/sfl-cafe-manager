package com.sfl.task.cafe.messaging.orderprocessor;

import com.sfl.task.cafe.domain.orderprocess.Order;
import com.sfl.task.cafe.domain.orderprocess.OrderStatus;
import com.sfl.task.cafe.event.manager.TableAssignedToWaiterEvent;
import com.sfl.task.cafe.repository.orderprocessor.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.Optional;

@Component
public class IncomingMessageListener {
	private OrderRepository orderRepository;

	private RestTemplate restTemplate;

	public IncomingMessageListener(OrderRepository orderRepository, RestTemplate restTemplate) {
		this.orderRepository = orderRepository;
		this.restTemplate = restTemplate;
	}

	@StreamListener(OrderProcessChannels.TABLE_ASSIGNED_TO_WAITER)
	public void receiveTableAssignedToWaiterEvent(@Payload TableAssignedToWaiterEvent event) {

		Order loadedOrder = null;
		Optional<Order> order = orderRepository.findFirstByWaiterIdAndTableIdAndOrderByUpdatedAtDesc(event.getWaiterId(), event.getTableId());
		if(!order.isPresent()) {
			loadedOrder = new Order();
			loadedOrder.setTableId(event.getTableId());
			loadedOrder.setCreatedAt(new Date());
			loadedOrder.setStatus(OrderStatus.NEW);
		}
		else {
			loadedOrder = order.get();
		}
		loadedOrder.setUpdatedAt(new Date());
		loadedOrder.setWaiterId(event.getWaiterId());

		orderRepository.save(loadedOrder);
	}
}
