package com.sfl.task.cafe.messaging.orderprocessor;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface OrderProcessChannels {
	String TABLE_ASSIGNED_TO_WAITER = "TableAssignedToWaiterEvent";

	@Input
	SubscribableChannel TableAssignedToWaiterIn();

	@Input
	SubscribableChannel orderItemDeclinedIn();

	@Output
	MessageChannel orderCompletedOut();
}
