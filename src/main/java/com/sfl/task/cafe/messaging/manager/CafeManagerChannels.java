package com.sfl.task.cafe.messaging.manager;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;

public interface CafeManagerChannels {
    String TABLE_ASSIGNED_TO_WAITER = "tableAssignedToWaiterIn";

    @Output
    MessageChannel tableAssignedToWaiterOut();

    @Input
    SubscribableChannel creditApplicationNumberGeneratedIn();

}
