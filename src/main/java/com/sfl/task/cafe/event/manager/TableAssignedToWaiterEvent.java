package com.sfl.task.cafe.event.manager;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

@Getter
@Setter
public class TableAssignedToWaiterEvent implements Serializable {
    private UUID id;
    private Date creationTime;
    private Long tableId;
    private Long waiterId;

    public TableAssignedToWaiterEvent(Date creationTime, Long tableId, Long waiterId) {
        super();
        this.creationTime = creationTime;
        this.tableId = tableId;
        this.waiterId = waiterId;
    }

    public TableAssignedToWaiterEvent() {
        this.id = UUID.randomUUID();
    }

    @Override
    public String toString() {
        return "CreditApplicationNumberGeneratedEvent{" +
                "id=" + id +
                ", creationTime=" + creationTime +
                ", tableId='" + tableId + '\'' +
                ", waiterId='" + waiterId + '\'' +
                '}';
    }
}
