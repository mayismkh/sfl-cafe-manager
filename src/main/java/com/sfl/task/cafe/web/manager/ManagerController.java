package com.sfl.task.cafe.web.manager;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.domain.manager.UserRole;
import com.sfl.task.cafe.event.manager.TableAssignedToWaiterEvent;
import com.sfl.task.cafe.messaging.manager.CafeManagerChannels;
import com.sfl.task.cafe.repository.manager.CafeTableRepository;
import com.sfl.task.cafe.repository.manager.UserRepository;
import com.sfl.task.cafe.service.manager.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping(path = "manager")
@ComponentScan(basePackages = {"com.sfl.task.cafe.repository", "com.sfl.task.cafe.service", "com.sfl.task.cafe.messaging.manager"})
public class ManagerController {
    private final static Logger LOGGER = LoggerFactory.getLogger(ManagerController.class);

    private final CafeTableRepository cafeTableRepository;
    private final UserRepository userRepository;
    //private final CafeManagerChannels cafeManagerChannels;
    private final IUserService userService;

    @Value("${nextProcessStepUrl}")
    private String nextProcessStepUrl;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public ManagerController(final CafeTableRepository cafeTableRepository,
                             final UserRepository userRepository,
                             final IUserService userService
                             //final CafeManagerChannels cafeManagerChannels
                             ) {
        this.cafeTableRepository = cafeTableRepository;
        this.userRepository = userRepository;
        //this.cafeManagerChannels = cafeManagerChannels;
        this.userService = userService;
    }

    /**
     * Return All users "manager/user/all".
     * URL request {"/manager/user", "/user/manager/", "/manager/user/all"},
     * GET.
     *
     * @return object of class {@link ModelAndView}.
     */
    @RequestMapping(
            value = {"", "/", "/all"},
            method = RequestMethod.GET
    )
    public ModelAndView viewAllManagers() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", this.userService.getManagers());
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.setViewName("user/manager/all");
        return modelAndView;
    }


    @RequestMapping(
            value = {"/user/{username}"},
            method = RequestMethod.GET)

    public ModelAndView index(@PathVariable String username) {

        LOGGER.info("Received a request for a user: " + username);
        User user = userRepository.findByUsername(username);

        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("user", user);
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.addObject("roles", UserRole.values());
        modelAndView.setViewName("user/manager/one");
        return modelAndView;
    }


    @PostMapping("/manager/waiter/{waiterId}/table/{tableId}/assign")
    public ModelAndView assignTableToWaiter(@PathVariable Long waiterId,
                                      @PathVariable Long tableId,
                                      Model model) {
        LOGGER.info("Received assignTableToWaiter waiter: " + waiterId + "table: " + tableId);

        // We are sending a TableAssignedToWaiterEvent
        TableAssignedToWaiterEvent event = new TableAssignedToWaiterEvent();
        event.setTableId(tableId);
        event.setWaiterId(waiterId);
//        cafeManagerChannels.tableAssignedToWaiterOut()
//                .send(MessageBuilder.withPayload(event).build());
        LOGGER.info("Sent " + event.toString());

        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("waiterId", waiterId);
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.addObject("roles", UserRole.values());
        modelAndView.setViewName("order/waiter/all");
        return modelAndView;
    }

}
