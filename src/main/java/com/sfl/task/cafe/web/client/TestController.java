package com.sfl.task.cafe.web.client;

import com.sfl.task.cafe.service.IHomeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Controller class for Test pages. All users can call this page.
 */
@Controller
@ComponentScan(basePackages = "com.sfl.task.cafe.service")
public final class TestController {

    /**
     * Object to work with home page
     */
    private final IHomeService homeService;

    /**
     * Ctor
     *
     * @param homeService service object for home page.
     */
    @Autowired
    public TestController(final IHomeService homeService) {
        this.homeService = homeService;
    }

    /**
     * Return Page "client/test".
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = "/test",
            method = RequestMethod.GET
    )
    public ModelAndView getTestPage() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("home/test");
        return modelAndView;
    }
}
