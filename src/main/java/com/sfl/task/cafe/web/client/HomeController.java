package com.sfl.task.cafe.web.client;

import com.sfl.task.cafe.domain.inventory.Category;
import com.sfl.task.cafe.domain.inventory.Item;
import com.sfl.task.cafe.domain.inventory.Product;
import com.sfl.task.cafe.service.inventory.IInventoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Optional;


/**
 * Class Controller for Home Page.No password required.
 */
@Controller
@ComponentScan("com.sfl.task.cafe.service.inventory")
//@ComponentScan(basePackages = "com.sfl.task.cafe.service")
public final class HomeController {
    /**
     * Service object to call inventory functional
     */
    private final IInventoryService inventoryService;

    /**
     * @param inventoryService
     */
    @Autowired
    public HomeController(
            final IInventoryService inventoryService
    ) {
        this.inventoryService = inventoryService;
    }

    /**
     * Return to hame page of site "client/home".
     * URL {"", "/", "/index", "/home"}, method GET.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = {"", "/", "/index", "/home"},
            method = RequestMethod.GET
    )
    public ModelAndView home() {
        final ModelAndView modelAndView = new ModelAndView();
        var categories = this.inventoryService.getCategories();
//        var brands = this.inventoryService.getAllBrands();
        modelAndView.addObject("categories", categories);
//        modelAndView.addObject("brands", brands);
//        modelAndView.addObject("brands_length", brands.size());
        modelAndView.setViewName("home/home");
        return modelAndView;
    }

    /**
     * Return to "client/category" with products from category url.
     * URL "/category/{url}", method GET.
     */
    @RequestMapping(
            value = "/category/{url}",
            method = RequestMethod.GET
    )
    public ModelAndView viewProductsInCategory(@PathVariable("url") final String url) {
        final ModelAndView modelAndView = new ModelAndView();
        Optional<Category> category = this.inventoryService.getCategoryByUrl(url);
        if(category.isEmpty()){
            throw new NullPointerException("No category found by url: " + url);
        }
        modelAndView.addObject("category", category.get());
        modelAndView.addObject("products", this.inventoryService.getProductsByCategoryId(category.get().getId()));
        modelAndView.setViewName("category/one");
        return modelAndView;
    }

    /**
     * Return "client/products" page with all products
     * URL "/product/all", method GET.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = "/product/all",
            method = RequestMethod.GET
    )
    public ModelAndView viewAllProducts() {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("items", this.inventoryService.getAllItems());
        modelAndView.setViewName("product/list");
        return modelAndView;
    }

    /**
     * Return product page "client/product" by unique URL,
     * URL "/product/{url}", method GET.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = "/product/{url}",
            method = RequestMethod.GET
    )
    public ModelAndView viewProduct(@PathVariable("url") final String url) {
        Item item;
        item = this.inventoryService.getItemByProductUrl(url);
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("item", item);
        modelAndView.setViewName("product/list");
        return modelAndView;
    }

    /**
     * Forwarded to manager page with users.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = {"/manager", "/manager/"},
            method = RequestMethod.GET
    )
    public String redirectToManagerPage() {
        return "redirect: user/manager/all";
    }

    /**
     * Forwarded to waiter page with orders.
     *
     * @return {@link ModelAndView}.
     */
    @RequestMapping(
            value = {"/waiter", "/waiter/"},
            method = RequestMethod.GET
    )
    public String redirectToWaiterPage() {
        return "redirect: user/waiter/all";
    }
}
