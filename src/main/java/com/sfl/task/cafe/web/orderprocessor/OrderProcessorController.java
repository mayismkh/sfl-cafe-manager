package com.sfl.task.cafe.web.orderprocessor;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.domain.manager.UserRole;
import com.sfl.task.cafe.service.manager.IUserService;
import com.sfl.task.cafe.service.orderprocessor.IOrderProcessorService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.naming.AuthenticationException;

@Controller
@RequestMapping(path = "waiter/")
@ComponentScan(basePackages = "com.sfl.task.cafe.service")
public class OrderProcessorController {
    private final static Logger LOGGER = LoggerFactory.getLogger(OrderProcessorController.class);

    private final IOrderProcessorService orderProcessorService;
    private IUserService userService;

    @Value("${nextProcessStepUrl}")
    private String nextProcessStepUrl;

    @Autowired
    @SuppressWarnings("SpringJavaAutowiringInspection")
    public OrderProcessorController(IOrderProcessorService orderProcessorService,
                                    IUserService userService) {
        this.orderProcessorService = orderProcessorService;
        this.userService = userService;
    }

    /**
     * Return All tables "waiter/tables".
     * URL request {"/tables", "/tables/", "/tables/all"},
     * GET.
     *
     * @return object of class {@link ModelAndView}.
     */
    @RequestMapping(
            value = {"/tables", "/tables/", "/tables/all"},
            method = RequestMethod.GET
    )
    public ModelAndView viewWaiterTables(Model model) throws AuthenticationException {

        User authUser = this.userService.getAuthenticatedUser();
        if(authUser == null){
            throw new AuthenticationException("There is no logged in user");
        }
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", this.orderProcessorService.getWaiterTablesByWaiterId(authUser.getId()));
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", authUser);
        modelAndView.setViewName("table/all");
        return modelAndView;
    }

    @PostMapping("/waiter/{waiterId}/tables/{tableId}/order/create")
    public ModelAndView createOrderForTables(Model model, @PathVariable Long waiterId, @PathVariable Long tableId) {
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", this.orderProcessorService.getWaiterTablesByWaiterId(waiterId));
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.setViewName("user/waiter/allTables");
        return modelAndView;
    }

    @GetMapping({"/", "/all"})
    public ModelAndView viewWaitersList(Model model) {
        LOGGER.info("Received a request for a all users");
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("users", this.userService.getWaiters());
        modelAndView.addObject("manager_role", UserRole.MANAGER);
        modelAndView.addObject("waiter_role", UserRole.WAITER);
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.setViewName("user/waiter/all");
        return modelAndView;
    }

    @GetMapping({"/transaction", "/transaction/all"})
    public ModelAndView viewOrdersList(Model model) {
        LOGGER.info("Received a request for a all orders");
        final ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("transactions", this.orderProcessorService.getAllTransactions());
        modelAndView.addObject("auth_user", this.userService.getAuthenticatedUser());
        modelAndView.setViewName("order/waiter/all");
        return modelAndView;
    }

}
