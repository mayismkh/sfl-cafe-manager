package com.sfl.task.cafe.config;

import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.dao.annotation.PersistenceExceptionTranslationPostProcessor;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * Class for main configuration for Spring:
 * DataSource,
 * JpaVendorAdapter,
 * JpaTransactionManager,
 * BeanPostProcessor,
 * CommonsMultipartResolver.
 */
@Configuration
@EnableTransactionManagement
//@EnableJpaRepositories(basePackages = "com.sfl.task.cafe.repository")
public class RootConfig {

    /**
     * Converted all JPA
     * or Hibernate exceptions to Spring exception.
     *
     * @return PersistenceExceptionTranslationPostProcessor.
     * interface realization
     */
    @Bean
    public BeanPostProcessor persistenceTranslation() {
        return new PersistenceExceptionTranslationPostProcessor();
    }

    /**
     * Return the object of class CommonsMultipartResolver,
     * which is saves temporary files in servlet container
     */
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        return new CommonsMultipartResolver();
    }

    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigInDev() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
