package com.sfl.task.cafe.config;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.filter.CharacterEncodingFilter;
import org.springframework.web.servlet.DispatcherServlet;
import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

import javax.servlet.Filter;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;

/**
 * Servlet Dispatcher, which is responsible for Spring MVC initializer and URL mapping.
 * class extends the AbstractAnnotationConfigDispatcherServletInitializer.
 *
 * @see WebConfig
 * @see RootConfig
 * @see SecurityConfig
 * @see SecurityInitializer
 */
@EntityScan("com.sfl.task.cafe.domain")
public class AppInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * Return configuration where initialized ViewResolver.
     *
     * @return array of properties classes
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class<?>[] { WebConfig.class };
    }

    /**
     * Return configuration which initialized Beans.
     *
     *@return {@link RootConfig} and {@link SecurityConfig}.
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class<?>[] {
                RootConfig.class,
                DatabaseConfig.class,
                SecurityConfig.class
        };
    }

    /**
     * Configured to map to "/" servlet that is why all requests will be catched by spring servled dispather
     *
     * @return array of String.
     */
    @Override
    protected String[] getServletMappings() {
        return new String[] { "/" };
    }

    /**
     * Sesson configuration.
     *
     * @throws ServletException
     */
    @Override
    public void onStartup(final ServletContext servletContext) throws ServletException {
        super.onStartup(servletContext);
        final Filter filter = new CharacterEncodingFilter();
        final FilterRegistration.Dynamic dynamic = servletContext.addFilter("encodingFilter", filter);
        dynamic.setInitParameter("encoding", "UTF-8");
        dynamic.setInitParameter("forceEncoding", "true");
        dynamic.addMappingForUrlPatterns(null, true, "/*");
    }

    /**
     * Enable NoHandlerFound exception.
     */
    @Override
    protected DispatcherServlet createDispatcherServlet(final WebApplicationContext context) {
        final DispatcherServlet dispatcherServlet =
                (DispatcherServlet) super.createDispatcherServlet(context);
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(true);
        return dispatcherServlet;
    }
}
