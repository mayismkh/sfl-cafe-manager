package com.sfl.task.cafe.config;

import com.sfl.task.cafe.domain.manager.UserRole;
import com.sfl.task.cafe.service.manager.IUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.authentication.configuration.EnableGlobalAuthentication;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;


/**
 *Configuration class for Spring Security.
 *
 * @see UserDetailsService
 * @see SecurityInitializer
 */

@Configuration
//@EnableWebSecurity
@EnableGlobalAuthentication
@ComponentScan("com.sfl.task.cafe.service.manager")
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    /**
     * URL prefix for managers.
     */
    private static final String MANAGER_REQUEST_URl = "/manager/**";

    /**
     * URL prefix for waiters
     */
    private static final String WAITER_REQUEST_URl = "/waiter/**";

    /**
     * URL for login request
     */
    private static final String LOGIN_URL = "/login";

    /**
     * The user name in autorization page
     */
    private static final String USERNAME = "username";

    /**
     * The user password field in authorization page
     */
    private static final String PASSWORD = "password";

    /**
     * URL request when access denied during
     */
    private static final String ACCESS_DENIED_PAGE = "/forbidden_exception";

    /**
     * Service object for working with registered users
     */
    private final IUserService userDetailsService;

    @Autowired
    public SecurityConfig(IUserService userDetailsService) {
        this.userDetailsService = userDetailsService;
    }

//    @Autowired
//    public SecurityConfig(UserDetailsService userDetailsService) {
//        this.userDetailsService = userDetailsService;
//    }

    /**
     * configuration of users access rules to pages.
     * To resource "{@value MANAGER_REQUEST_URl}", only users with MANAGER role can have access.
     * To resource "{@value WAITER_REQUEST_URl}", users with MANAGER and WAITER roles
     */
    @Override
    protected void configure(final HttpSecurity httpSecurity)
            throws Exception {
//        httpSecurity
//                .csrf().disable();
//        httpSecurity
//                .authorizeRequests()
//                .antMatchers("/").permitAll()
//                .anyRequest().authenticated();
//        httpSecurity
//                .formLogin()
//                .loginPage(LOGIN_URL)
//                .defaultSuccessUrl("/home")
//                .failureUrl("/login?error")
//                .permitAll()
//                .and()
//                .logout()
//                .invalidateHttpSession(true)
//                .logoutSuccessUrl("/login?logout")
//                .permitAll();

        httpSecurity
                .logout()
                .invalidateHttpSession(false)
                .and()
                .authorizeRequests()
                .antMatchers(MANAGER_REQUEST_URl)
                .hasRole(UserRole.MANAGER.name())
                .antMatchers(WAITER_REQUEST_URl)
                .hasAnyRole(
                        UserRole.WAITER.name(),
                        UserRole.MANAGER.name()
                )
                .anyRequest().permitAll()
                .and()
                .formLogin()
                .loginPage(LOGIN_URL)
                .usernameParameter(USERNAME)
                .passwordParameter(PASSWORD)
                .defaultSuccessUrl("/", false)
                .and()
                .exceptionHandling().accessDeniedPage(ACCESS_DENIED_PAGE).and()
                .csrf().disable();
    }

//    /**
//     * User config with its roles. Users will be loaded from DB,
//     * using methods implmented in UserDetailsService interface.
//     *
//     * @param builder AuthenticationManagerBuilder class object.
//     * @throws Exception AuthenticationManagerBuilder.
//     */
//    @Override
//    protected void configure(final AuthenticationManagerBuilder builder) throws Exception {
////        builder.inMemoryAuthentication()
////                .withUser("user").password("password").roles("WAITER").and()
////                .withUser("admin").password("password").roles("WAITER", "MANAGER");
//
//        builder.userDetailsService((UserDetailsService)this.userDetailsService);
//    }

//    @Override
//    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
//        clients
//                .inMemory()
//                .withClient("test")
//                .scopes("read", "write")
//                .authorities(Roles.ADMIN.name(), Roles.USER.name())
//                .authorizedGrantTypes("password", "refresh_token")
//                .secret("secret")
//                .accessTokenValiditySeconds(1800);
//    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

        PasswordEncoder encoder = PasswordEncoderFactories.createDelegatingPasswordEncoder();
        auth.inMemoryAuthentication()
                .withUser("user")
                .password(encoder.encode("123456"))
                .roles("WAITER").and()
                .withUser("admin")
                .password(encoder.encode("123456"))
                .roles("WAITER", "MANAGER");
    }

//    @Bean
//    @Override
//    public UserDetailsService userDetailsService() {
//
//        //WAITER Role
//        UserDetails theUser = User.withUsername("user")
//                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
//                .password("123456").roles("WAITER").build();
//
//        //MANAGER Role
//        UserDetails theManager = User.withUsername("admin")
//                .passwordEncoder(PasswordEncoderFactories.createDelegatingPasswordEncoder()::encode)
//                .password("654321").roles("MANAGER").build();
//
//
//        InMemoryUserDetailsManager userDetailsManager = new InMemoryUserDetailsManager();
//
//        userDetailsManager.createUser(theUser);
//        userDetailsManager.createUser(theManager);
//
//        return userDetailsManager;
//
//    }
}
