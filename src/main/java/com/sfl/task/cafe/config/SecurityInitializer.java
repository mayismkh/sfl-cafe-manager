package com.sfl.task.cafe.config;

import org.springframework.security.web.context.AbstractSecurityWebApplicationInitializer;

/**
 * Implementation of AbstractSecurityWebApplicationInitializer.
 * It needs to be sure that security configuration is attached to main context
 * URL goes true security level
 *
 * @see SecurityConfig
 * @see RootConfig
 * @see WebConfig
 */
public class SecurityInitializer extends AbstractSecurityWebApplicationInitializer {
}
