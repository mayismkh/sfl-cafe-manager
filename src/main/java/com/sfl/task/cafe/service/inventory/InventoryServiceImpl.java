package com.sfl.task.cafe.service.inventory;

import com.sfl.task.cafe.domain.inventory.Brand;
import com.sfl.task.cafe.domain.inventory.Category;
import com.sfl.task.cafe.domain.inventory.Item;
import com.sfl.task.cafe.domain.inventory.Product;
import com.sfl.task.cafe.repository.inventory.BrandRepository;
import com.sfl.task.cafe.repository.inventory.CategoryRepository;
import com.sfl.task.cafe.repository.inventory.ItemRepository;
import com.sfl.task.cafe.repository.inventory.ProductRepository;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

@Service
@ComponentScan("com.sfl.task.cafe.repository.inventory")
public class InventoryServiceImpl implements IInventoryService {

    /**
     * Realisation of interface {@link InventoryServiceImpl}
     */
    private final BrandRepository brandRepository;
    private final CategoryRepository categoryRepository;
    private final ItemRepository itemRepository;
    private final ProductRepository productRepository;


    @Autowired
    public InventoryServiceImpl(final BrandRepository brandRepository,
                           final CategoryRepository categoryRepository,
                           final ItemRepository itemRepository,
                           final ProductRepository productRepository) {
        this.brandRepository = brandRepository;
        this.categoryRepository = categoryRepository;
        this.itemRepository = itemRepository;
        this.productRepository = productRepository;
    }

    @Override
    public Optional<Category> getCategoryByUrl(String url) throws IllegalArgumentException,  NullPointerException {
        if(!StringUtils.hasLength(url)){
            throw new IllegalArgumentException("url not specified");
        }

        Optional<Category> category = this.categoryRepository.findByUrl(url);
        if(category.isEmpty()) {
            throw new NullPointerException("There id no category by Url: " + url);
        }
        return category;
    }

    @Override
    public Collection<Product> getProductsByCategoryId(Long categoryId) {
        if(categoryId == null){
            throw new IllegalArgumentException("category id not specified");
        }

        Collection<Product> products = this.productRepository.findByCategoryId(categoryId);
        if(ArrayUtils.isEmpty(products.toArray())) {
            throw new NullPointerException("There id no products by category Id: " + categoryId);
        }
        return products;
    }

    @Override
    public Collection<Product> getProductsByCategoryUrl(String url) {
        if(StringUtils.isEmpty(url)){
            throw new IllegalArgumentException("url not specified");
        }

        Collection<Product> products = this.productRepository.findByCategoriesContaining(url);
        if(ArrayUtils.isEmpty(products.toArray())) {
            throw new NullPointerException("There id no products by Url: " + url);
        }
        return products;
    }

    @Override
    public Collection<Category> getCategories() {

        Collection<Category> target = new ArrayList<>();
        this.categoryRepository.findAll().forEach(target::add);

//        if(ArrayUtils.isEmpty(target.toArray())) {
//            throw new NullPointerException("There id no loaded categories");
//        }
        return target;
    }

    @Override
    public Collection<Product> getAllProducts() {
        Collection<Product> target = new ArrayList<>();
        this.productRepository.findAll().forEach(target::add);

//        if(ArrayUtils.isEmpty(target.toArray())) {
//            throw new NullPointerException("There id no loaded items");
//        }
        return target;

    }

    @Override
    public Collection<Brand> getAllBrands() {
        Collection<Brand> target = new ArrayList<>();
        this.brandRepository.findAll().forEach(target::add);

//        if(ArrayUtils.isEmpty(target.toArray())) {
//            throw new NullPointerException("There id no loaded brands");
//        }
        return target;
    }

    @Override
    public Collection<Item> getAllItems() {
        Collection<Item> target = new ArrayList<>();
        this.itemRepository.findAll().forEach(target::add);

//        if(ArrayUtils.isEmpty(target.toArray())) {
//            throw new NullPointerException("There id no loaded items");
//        }
        return target;
    }

    @Override
    public Item getItemByProductUrl(String url) {
        return null;
    }
}
