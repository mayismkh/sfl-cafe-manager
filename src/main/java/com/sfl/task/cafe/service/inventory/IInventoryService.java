package com.sfl.task.cafe.service.inventory;

import com.sfl.task.cafe.domain.inventory.Brand;
import com.sfl.task.cafe.domain.inventory.Category;
import com.sfl.task.cafe.domain.inventory.Item;
import com.sfl.task.cafe.domain.inventory.Product;

import java.util.Collection;
import java.util.Optional;

public interface IInventoryService {
    Optional<Category> getCategoryByUrl(String url);

    Collection<Product>  getProductsByCategoryId(Long id);

    Collection<Product> getProductsByCategoryUrl(String url);

    Collection<Category> getCategories();

    Collection<Product> getAllProducts();

    Collection<Brand> getAllBrands();

    Collection<Item> getAllItems();

    Item getItemByProductUrl(String url);
}
