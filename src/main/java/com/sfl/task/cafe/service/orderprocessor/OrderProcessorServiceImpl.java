package com.sfl.task.cafe.service.orderprocessor;

import com.sfl.task.cafe.domain.inventory.Category;
import com.sfl.task.cafe.domain.manager.CafeTable;
import com.sfl.task.cafe.domain.orderprocess.Order;
import com.sfl.task.cafe.domain.orderprocess.OrderStatus;
import com.sfl.task.cafe.domain.orderprocess.Transaction;
import com.sfl.task.cafe.repository.manager.CafeTableRepository;
import com.sfl.task.cafe.repository.orderprocessor.OrderItemRepository;
import com.sfl.task.cafe.repository.orderprocessor.OrderRepository;
import com.sfl.task.cafe.repository.orderprocessor.TransactionRepository;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.InvalidParameterException;
import java.util.*;

@Service
@ComponentScan(basePackages = "com.sfl.task.cafe.repository")
public class OrderProcessorServiceImpl implements IOrderProcessorService {

    /**
     * Realization of interface {@link OrderItemRepository}
     */
    private final OrderItemRepository orderItemRepository;
    /**
     * Realization of interface {@link OrderRepository}
     */
    private final OrderRepository orderRepository;
    /**
     * Realization of interface {@link TransactionRepository}
     */
    private final TransactionRepository transactionRepository;
    /**
     * Realization of interface {@link CafeTableRepository}
     */
    private final CafeTableRepository cafeTableRepository;

    @Autowired
    public OrderProcessorServiceImpl(final OrderItemRepository orderItemRepository,
                                     final OrderRepository orderRepository,
                                     final TransactionRepository transactionRepository,
                                     final CafeTableRepository cafeTableRepository) {
        this.orderItemRepository = orderItemRepository;
        this.orderRepository = orderRepository;
        this.transactionRepository = transactionRepository;
        this.cafeTableRepository = cafeTableRepository;
    }

//    /**
//     * Delete all personnel
//     */
//    @Override
//    @Transactional
//    public void removePersonnel() {
//        final Collection<User> personnel = getPersonnel();
//        if (ArrayUtils.isNotEmpty(personnel.toArray())) {
//            final User mainAdmin = getMainManager();
//            personnel.remove(mainAdmin);
//            this.repository.deleteAll(personnel);
//        }
//    }

    @Override
    @Transactional(readOnly = true)
    public Collection<CafeTable> getWaiterTablesByWaiterId(Long waiterId)
            throws IllegalArgumentException, InvalidParameterException {
        if (waiterId != null) {
            final Collection<CafeTable> tables = this.cafeTableRepository.findByWaiterId(waiterId);
            if (ArrayUtils.isEmpty(tables.toArray())) {
                throw new InvalidParameterException("Can't find user by waiter Id " + waiterId + "!");
            }
            return tables;
        }
        throw new IllegalArgumentException("No waiter Id!");
    }

    @Override
    @Transactional
    public Order createOrderForTable(Long tableId, Long waiterId)
            throws IllegalArgumentException, InvalidParameterException {
        if (tableId != null) {
            final Optional<CafeTable> table = this.cafeTableRepository.findById(tableId);
            if (table.isEmpty()) {
                throw new InvalidParameterException("Can't find table by table Id " + tableId + "!");
            }

            Order order = new Order();
            order.setTableId(tableId);
            order.setWaiterId(waiterId);
            order.setOrderCode(UUID.randomUUID().toString().replace("-", ""));
            order.setStatus(OrderStatus.NEW);
            order.setCreatedAt(new Date());
            return this.orderRepository.save(order);
        }
        throw new IllegalArgumentException("No table by Id!");
    }

    @Override
    public CafeTable getTableByNumber(Integer tableNumber) {
        return null;
    }

    @Override
    @Transactional
    public Collection<Transaction> getAllTransactions() {
        Collection<Transaction> target = new ArrayList<>();
        this.transactionRepository.findAll().forEach(target::add);

        return target;

    }
}
