package com.sfl.task.cafe.service.manager;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.domain.manager.UserRole;
import com.sfl.task.cafe.repository.manager.UserRepository;
import org.apache.commons.lang.ArrayUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@Service
public class UserServiceImpl implements IUserService, UserDetailsService {

    /**
     * Realisation of interface {@link UserRepository}
     */
    private final UserRepository repository;

    @Autowired
    public UserServiceImpl(final UserRepository repository) {
        this.repository = repository;
    }

    /**
     * Return User by name
     *
     * @throws IllegalArgumentException,  when empty input parameter
     * @throws NullPointerException, if no user found by name
     */
    @Override
    @Transactional(readOnly = true)
    public User getByName(final String name) throws IllegalArgumentException, NullPointerException {
        if (StringUtils.hasLength(name)) {
            final User user = this.repository.findByFirstName(name);
            if (user == null) {
                throw new NullPointerException("Can't find user by name " + name + "!");
            }
            return user;
        }
        throw new IllegalArgumentException("No user name!");
    }

    /**
     * Return User  by login user name
     */
    @Override
    @Transactional(readOnly = true)
    public User getByUsername(final String username) throws IllegalArgumentException, NullPointerException {
        if (!StringUtils.hasLength(username)) {
            throw new IllegalArgumentException("No username!");
        }
        final User user = this.repository.findByUsername(username);
        if (user == null) {
            throw new NullPointerException("Can't find user by username " + username + "!");
        }
        return user;
    }

    /**
     * Return main manager. Read only
     */
    @Override
    @Transactional(readOnly = true)
    public User getMainManager() throws NullPointerException {
        final User user = new ArrayList<>(getManagers()).get(0);
        if (user == null) {
            throw new NullPointerException("Can't find manager!");
        }
        return user;
    }

    /**
     * Return all managers
     */
    @Override
    @Transactional(readOnly = true)
    public Collection<User> getManagers() {
        return this.repository.findAllByRole(UserRole.MANAGER);
    }

    /**
     * Return all waiters
     */
    @Override
    @Transactional(readOnly = true)
    public Collection<User> getWaiters() {
        return this.repository.findAllByRole(UserRole.WAITER);
    }

    /**
     * Return all personnel
     */
    @Override
    @Transactional(readOnly = true)
    public Collection<User> getPersonnel() {
        final List<User> users = new ArrayList<>();
        users.addAll(getManagers());
        users.addAll(getWaiters());
        return users;
    }

    /**
     * Return authenticated in the system user
     */
    @Override
    @Transactional(readOnly = true)
    public User getAuthenticatedUser() {
        User user;
        try {
            final SecurityContext context = SecurityContextHolder.getContext();
            final Authentication authentication = context.getAuthentication();
            user = (User) authentication.getPrincipal();
        } catch (Exception ex) {
            ex.printStackTrace();
            user = null;
        }
        return user;
    }

    /**
     * Delete user by name
     */
    @Override
    @Transactional
    public void removeByName(final String name) {
        if (StringUtils.hasLength(name)) {
            this.repository.deleteByFirstName(name);
        }
    }

    /**
     * Delete users by role
     */
    @Override
    @Transactional
    public void removeByRole(final UserRole role) {
        if (role != null) {
            this.repository.deleteAllByRole(role);
        }
    }

    /**
     * Delete all personnel
     */
    @Override
    @Transactional
    public void removePersonnel() {
        final Collection<User> personnel = getPersonnel();
        if (ArrayUtils.isNotEmpty(personnel.toArray())) {
            final User mainAdmin = getMainManager();
            personnel.remove(mainAdmin);
            this.repository.deleteAll(personnel);
        }
    }

    /**
     * Return corresponding with logged in user
     * implementation of {@link UserDetailsService} interface.
     *
     * @throws UsernameNotFoundException
     */
    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
        try {
            return getByUsername(username);
        } catch (IllegalArgumentException | NullPointerException ex) {
            throw new UsernameNotFoundException(ex.getMessage(), ex);
        }
    }
}
