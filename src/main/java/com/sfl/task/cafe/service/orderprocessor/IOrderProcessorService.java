package com.sfl.task.cafe.service.orderprocessor;

import com.sfl.task.cafe.domain.manager.CafeTable;
import com.sfl.task.cafe.domain.orderprocess.Order;
import com.sfl.task.cafe.domain.orderprocess.Transaction;

import java.util.Collection;

public interface IOrderProcessorService {

    /**
     * Get Waiters tables list
     * @param waiterId
     * @return
     */
    Collection<CafeTable> getWaiterTablesByWaiterId(Long waiterId);

    /**
     * Get tables By number
     * @param tableNumber
     * @return
     */
    CafeTable getTableByNumber(Integer tableNumber);

    /**
     * Get All transactions
     * @return
     */
    Collection<Transaction> getAllTransactions();

    /**
     * Create order to table for served by current waiter
     * @param tableId
     * @param waiterId
     * @return the new created order
     */
    Order createOrderForTable(Long tableId, Long waiterId);
}
