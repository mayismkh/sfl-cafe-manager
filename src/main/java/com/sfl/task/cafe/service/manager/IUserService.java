package com.sfl.task.cafe.service.manager;

import com.sfl.task.cafe.domain.manager.User;
import com.sfl.task.cafe.domain.manager.UserRole;

import java.util.Collection;

public interface IUserService {

    /**
     * Retrieves a list of Users
     */
    Collection<User> getPersonnel();

    /**
     * Return Registered user
     * only read mode
     */
    User getAuthenticatedUser();

    /**
     * Return One of managers
     * only read mode
     */
    User getMainManager();

    /**
     * Return User by name
     *
     * @param name User name to find
     * @return object of {@link User}  class
     */
    User getByName(String name);

    /**
     * Return User by name
     */
    User getByUsername(String username);

    /**
     * Return All Managers
     */
    Collection<User> getManagers();

    /**
     * Return All Waiter
     */
    Collection<User> getWaiters();

    /**
     * Remove user by name
     */
    void removeByName(String name);

    /**
     * Remove all users by user role
     *
     */
    void removeByRole(UserRole role);

    /**
     * Remove All personnel
     */
    void removePersonnel();
}
