package com.sfl.task.cafe.service.manager;

import com.sfl.task.cafe.domain.manager.CafeTable;


import java.util.List;

public interface ITableService {
    /**
     * Adds a new Table
     *
     */
    boolean addTable(CafeTable table);

    /**
     * Retrieves a list of tables
     */
    List<CafeTable> getTables();
}
