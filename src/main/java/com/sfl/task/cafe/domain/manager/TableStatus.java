package com.sfl.task.cafe.domain.manager;

/**
 * Possible table status enumeration
 */
public enum TableStatus {

    /**
     * table is Available
     */
    AVAILABLE(1),

    /**
     * table is Reserved
     */
    RESERVED(2),

    /**
     * Table is not available
     */
    UNAVAILABLE(3);

    private final int numStatus;

    TableStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumStatus() {
        return numStatus;
    }
}
