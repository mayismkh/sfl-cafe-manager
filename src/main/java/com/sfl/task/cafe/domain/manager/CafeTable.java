package com.sfl.task.cafe.domain.manager;

import com.sfl.task.cafe.domain.inventory.Category;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * CafeTables
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			number INT NOT NULL,
 * 			waiterId INT NULL,
 * 		    status TINYINT NOT NULL DEFAULT 0,
 * 			capacity INT NOT NULL,
 * 			place VARCHAR(50) NULL
 *
 */

@Getter
@Setter
@Entity(name = "cafeTables")
@Table(name = "cafeTables")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public final class CafeTable extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Table number to identify
     */
    @Column(name = "number", nullable = false)
    private Integer number;

    /**
     * The capacity of Table
     */
    @Column(name = "capacity", nullable = false)
    private Integer capacity;

    /**
     * Where is the table
     */
    @Column(name = "place", nullable = false)
    private String place;

    /**
     * Waiter attached to table
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "waiterId", referencedColumnName = "id")
    private User waiter;

    /**
     * Table Status
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TableStatus status = TableStatus.AVAILABLE;

    @Override
    public Long getEntityId() {
        return getId();
    }

}
