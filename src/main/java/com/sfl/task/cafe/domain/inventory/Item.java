package com.sfl.task.cafe.domain.inventory;

import lombok.*;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Items
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			productId INT NOT NULL,
 * 			brandId INT NOT NULL,
 * 			supplierId INT NOT NULL,
 * 			orderId INT NOT NULL,
 * 			stockCode VARCHAR(100) NOT NULL,
 * 			retailPrice DECIMAL NOT NULL DEFAULT 0,
 * 			discount DECIMAL NOT NULL DEFAULT 0,
 * 			price DECIMAL NOT NULL DEFAULT 0,
 * 			quantity INT NOT NULL DEFAULT 0,
 * 			sold INT NOT NULL DEFAULT 0,
 * 			available INT NOT NULL DEFAULT 0,
 * 			createdAt DATETIME NOT NULL,
 * 			updatedAt DATETIME NULL DEFAULT NULL,
 */
@Getter
@Setter
@Entity(name = "items")
@Table(name = "items")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Item extends BaseEntity<Long> {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The referenced product
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "productId", referencedColumnName = "id")
    private Product product;

    /**
     * The referenced brand
     */
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "brandId", referencedColumnName = "id")
    private Brand brand;

    /**
     * The referenced supplier id
     */
    @Column(name = "supplierId", nullable = false)
    private Long supplierId;

    /**
     * The order assosiated with item buyed from supplier
     */
    @Column(name = "orderId", nullable = false)
    private Long orderId;

    /**
     * The item code in stock
     */
    @Column(name = "stockCode", nullable = false, length = 100)
    private String stockCode = "";

    /**
     * retail Price for sale
     */
    @Column(name = "retailPrice", nullable = false)
    private Double retailPrice;

    /**
     * The discount for item
     */
    @Column(name = "discount", nullable = false)
    private Double discount;

    /**
     * The purchased price of item
     */
    @Column(name = "price", nullable = false)
    private Double price;

    /**
     * The current  of Product
     */
    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    /**
     * The count of sold items
     */
    @Column(name = "sold", nullable = false)
    private Integer sold;

    /**
     * The count of available items in stock
     */
    @Column(name = "available", nullable = false)
    private Integer available;

    /**
     * The created time
     */
    @Column(name = "createdAt", nullable = false)
    private Date createdAt;

    /**
     * The updated time
     */
    @Column(name = "updatedAt", nullable = false)
    private Date updatedAt;

    @Override
    public Long getEntityId() {
        return null;
    }
}
