package com.sfl.task.cafe.domain.orderprocess;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;

/**
 * Orders
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			userId INT NOT NULL,
 * 			orderCode VARCHAR(100) NOT NULL,
 * 			cafeTableId INT NOT NULL,
 * 			status TINYINT NOT NULL,
 * 			total DECIMAL NOT NULL DEFAULT 0,
 * 			discount DECIMAL NOT NULL DEFAULT 0,
 * 			ItemDiscount DECIMAL NOT NULL DEFAULT 0,
 * 			paidTotal DECIMAL NOT NULL DEFAULT 0,
 * 			createdAt DATETIME NOT NULL,
 * 			updatedAt DATETIME NULL,
 */
@Getter
@Setter
@Entity(name = "orders")
@Table(name = "orders")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public final class Order extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "cafeTableId", nullable = false)
    private Long tableId;

    @Column(name = "userId", nullable = false)
    private Long waiterId;

    @Column(name = "orderCode", nullable = false, length = 100)
    private String orderCode;

    /**
     * The full / total price. without any discount.
     */
    @Column(name = "total", nullable = false)
    private Double total;

    /**
     * The total discount for personal discount.
     */
    @Column(name = "discount", nullable = false)
    private Double discount;

    /**
     * Ordered Items total discount.
     */
    @Column(name = "ItemDiscount", nullable = false)
    private Double ItemDiscount;

    /**
     * The paid by customer total price
     */
    @Column(name = "paidTotal", nullable = false)
    private Double paidTotal;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "updatedAt")
    private Date updatedAt;

    /**
     * Table Status
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private OrderStatus status = OrderStatus.NEW;

//    /**
//     * The list of products connected with current category
//     */
//    @OneToMany(
//            fetch = FetchType.LAZY,
//            mappedBy = "products_in_order",
//            cascade = CascadeType.ALL
//    )
//    private Collection<Transaction> products = new HashSet<>();

    @Override
    public Long getEntityId() {
        return getId();
    }

}
