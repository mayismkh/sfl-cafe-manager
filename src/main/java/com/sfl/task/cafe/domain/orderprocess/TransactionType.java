package com.sfl.task.cafe.domain.orderprocess;

/**
 * Possible Transaction types enumeration
 */
public enum TransactionType {
    /**
     * Transaction for sell
     */
    Sell(1),

    /**
     * Transaction for buy
     */
    Buy(2);

    private final int numType;

    TransactionType(int numType) {
        this.numType = numType;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumType() {
        return numType;
    }
}
