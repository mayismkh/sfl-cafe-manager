package com.sfl.task.cafe.domain.inventory;

import lombok.*;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.*;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

/**
 * Categories
 * 		    id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
 * 			parentCategoryId  INT NULL,
 * 			title varchar(255) NOT NULL,
 * 			url varchar(255) NOT NULL,
 * 			description varchar(500) NOT NULL
 */
@Entity(name = "categories")
@Table(name = "categories")
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Category extends BaseEntity<Long> {
    /**
     * The key for entity
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * Parent Category for current one
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "parentCategoryId", referencedColumnName = "id")
    private Category parentCategory;

    /**
     * The name of category
     */
    @Column(name = "title", nullable = false, length = 255)
    private String title = "";

    /**
     * The URL of category
     */
    @Column(name = "url", nullable = false, length = 255)
    private String url = "";

    /**
     * The description of category.
     */
    @Column(name = "description", length = 500)
    private String description = "";

    /**
     * The list of products connected with current category
     */
    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "productCategory",
            joinColumns = { @JoinColumn(name = "categoryId") },
            inverseJoinColumns = { @JoinColumn(name = "productId") }
    )
    private Set<Product> products = new HashSet<>();

    @Override
    public Long getEntityId() {
        return getId();
    }

    /**
     * Add product to current category
     *
     * @param product product to be added to category
     */
    public void addProduct(final Product product) {
        if (product != null) {
            this.products.add(product);
        }
    }

    /**
     * Add list of products to category.
     *
     * @param products the list of product, which will be added to category
     */
    public void addProducts(final Collection<Product> products) {
        if (isNotEmpty(products)) {
            this.products.addAll(products);
        }
    }

    /**
     * Delete product from category
     */
    public void removeProduct(final Product product) {
        if (product != null) {
            this.products.remove(product);
        }
    }

    /**
     * Remove the list of products from category
     */
    public void removeProducts(final Collection<Product> products) {
        if (isNotEmpty(products)) {
            this.products.removeAll(products);
        }
    }

    /**
     * Clear all products from category
     */
    public void clearProducts() {
        this.products.clear();
    }

    /**
     * Convert products to new collection and return
     */
    public Collection<Product> getProducts() {
        if(isNotEmpty(this.products)){
            return new ArrayList<>(this.products);
        }
        else{
            return new ArrayList<>();
        }
    }

    /**
     * Set collection of products for category
     */
    public void setProducts(final Collection<Product> products) {
        clearProducts();
        addProducts(products);
    }
}
