package com.sfl.task.cafe.domain.manager;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

/**
 *   id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
 * 			roleId  TINYINT NOT NULL,
 * 			firstName VARCHAR(50) NULL,
 * 			lastName VARCHAR(50) NULL,
 * 			userName VARCHAR(50) NULL,
 * 			mobile VARCHAR(50) NULL,
 * 			email VARCHAR(50) NULL,
 * 			passwordHash VARCHAR(500) NOT NULL,
 * 			registeredAt DATETIME NOT NULL,
 * 			lastLogin DATETIME NULL,
 */
@Getter
@Setter
@Entity(name = "users")
@Table(name = "users")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public final class User extends BaseEntity<Long> implements UserDetails{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "firstName", nullable = true, length = 50)
    private String firstName;

    @Column(name = "lastName", nullable = true, length = 50)
    private String lastName;

    @Column(name = "username", nullable = true, length = 50)
    private String username;

    @Column(name = "mobile", nullable = true, length = 50)
    private String phone;

    @Column(name = "email", nullable = true, length = 50)
    private String email;

    @Column(name = "passwordHash", length = 20)
    private String password;

    @Column(name = "registeredAt", nullable = false)
    private Date registeredAt;

    @Column(name = "lastLogin")
    private Date lastLogin;
    /**
     * User Role
     */
    @Column(name = "roleId", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private UserRole role = UserRole.WAITER;

    @Override
    public Long getEntityId() {
        return getId();
    }

    /**
     * Return user roles by object SimpleGrantedAuthority.
     * {@link UserDetails} implementation.
     *
     * @return {@link Collection} -list of roles
     */
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final List<GrantedAuthority> roles = new ArrayList<>();
        SimpleGrantedAuthority authority = new SimpleGrantedAuthority("ROLE_" + this.role.name());
        roles.add(authority);
        return roles;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
