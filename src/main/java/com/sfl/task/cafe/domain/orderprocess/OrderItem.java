package com.sfl.task.cafe.domain.orderprocess;

import com.sfl.task.cafe.domain.manager.User;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * OrderItems
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			productId INT NOT NULL,
 * 			itemId INT NOT NULL,
 * 			orderId INT NOT NULL,
 * 			stockCode VARCHAR(100) NOT NULL,
 * 			discount DECIMAL NOT NULL DEFAULT 0,
 * 			price DECIMAL NOT NULL DEFAULT 0,
 * 			quantity INT NOT NULL DEFAULT 0,
 * 			createdAt DATETIME NOT NULL,
 * 			updatedAt DATETIME NULL DEFAULT NULL,
 */
@Getter
@Setter
@Entity(name = "orderItems")
@Table(name = "orderItems")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class OrderItem extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The product
     */
    @Column(name = "productId", nullable = false)
    private Integer productId;

    /**
     * The id of Item
     */
    @Column(name = "itemId", nullable = false)
    private Integer itemId;

    /**
     * The Order item is included
     */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "orderId", referencedColumnName = "id")
    private Order order;

    /**
     * The stock code of product item
     */
    @Column(name = "stockCode", nullable = false, length = 100)
    private String stockCode = "";

    /**
     * The discount for order item
     */
    @Column(name = "discount", nullable = false)
    private Double discount;

    /**
     * The item price
     */
    @Column(name = "price", nullable = false)
    private Double price;

    /**
     * Product description.
     */
    @Column(name = "quantity", nullable = false)
    private Integer quantity;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "updatedAt")
    private Date updatedAt;

    @Override
    public Long getEntityId() {
        return getId();
    }
}
