package com.sfl.task.cafe.domain.manager;

/**
 * Possible user roles enumeration
 */
public enum UserRole {
    /**
     * Waiter role
     */
    WAITER(1),

    /**
     * cafe manager role
     */
    MANAGER(2),

    /**
     * Product Item SUPPLIER role
     */
    SUPPLIER(3);

    private int numRole;

    UserRole(int numRole) {
        this.numRole = numRole;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumRole() {
        return numRole;
    }
}
