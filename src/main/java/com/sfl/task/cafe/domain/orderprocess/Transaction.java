package com.sfl.task.cafe.domain.orderprocess;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.Date;

/**
 * Transactions
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			userId INT NOT NULL,
 * 			orderId INT NOT NULL,
 * 			status SMALLINT NOT NULL  DEFAULT 0,
 * 			type SMALLINT NOT NULL  DEFAULT 0,
 * 			mode SMALLINT NOT NULL  DEFAULT 0,
 * 			createdAt DATETIME NOT NULL,
 * 			updatedAt DATETIME NULL,
 */
@Getter
@Setter
@Entity(name = "transactions")
@Table(name = "transactions")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Transaction extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "userId", nullable = false)
    private Integer userId;

    @Column(name = "orderId", nullable = false)
    private Integer orderId;

    @Column(name = "createdAt")
    private Date createdAt;

    @Column(name = "updatedAt")
    private Date updatedAt;

    /**
     * Transaction status
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TransactionStatus status = TransactionStatus.New;

    /**
     * The type of transaction can be Buy or Sell.
     */
    @Column(name = "type", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TransactionType type = TransactionType.Buy;

    /**
     * The mode of the order transaction can be Cash, Online.
     */
    @Column(name = "mode", nullable = false)
    @Enumerated(EnumType.ORDINAL)
    private TransactionMode mode = TransactionMode.Cash;

    @Override
    public Long getEntityId() {
        return null;
    }
}
