package com.sfl.task.cafe.domain.inventory;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

/**
 * Brands
 * 		    id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
 * 			title  VARCHAR(255) NOT NULL,
 * 			url varchar(255) NOT NULL,
 * 			description varchar(500) NOT NULL
 */
@Entity(name = "brands")
@Table(name = "brands")
@Getter
@Setter
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Brand extends BaseEntity<Long> {
    /**
     * The key for entity
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * The name of brand
     */
    @Column(name = "title", nullable = false, length = 255)
    private String title = "";

    /**
     * The URL of brand
     */
    @Column(name = "url", nullable = false, length = 255)
    private String url = "";

    /**
     * The description of brand.
     */
    @Column(name = "description", length = 500)
    private String description = "";

    @Override
    public Long getEntityId() {
        return getId();
    }

 }
