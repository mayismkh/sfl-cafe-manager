package com.sfl.task.cafe.domain.orderprocess;

/**
 * Possible Transaction modes enumeration
 */
public enum TransactionMode {
    /**
     * by Cash
     */
    Cash(1),

    /**
     * complete Online
     */
    Online(2);

    private final int numMode;

    TransactionMode(int numMode) {
        this.numMode = numMode;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumMode() {
        return numMode;
    }

}
