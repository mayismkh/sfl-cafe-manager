package com.sfl.task.cafe.domain.orderprocess;

/**
 * Possible table status enumeration
 */
public enum TransactionStatus {
    /**
     * Transaction is New created
     */
    New(1),
    /**
     * Transaction is cancelled
     */
    Cancelled(2),
    /**
     * Transaction is Rejected
     */
    Rejected(3),
    /**
     * Transaction Success
     */
    Success(4);

    private final int numStatus;

    TransactionStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumStatus() {
        return numStatus;
    }
}
