package com.sfl.task.cafe.domain.inventory;

import lombok.*;
import org.seedstack.business.domain.BaseEntity;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.util.*;

import static org.hibernate.internal.util.collections.CollectionHelper.isNotEmpty;

/**
 * Products
 * 			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
 * 			type SMALLINT NOT NULL DEFAULT 0,
 * 			title VARCHAR(100) NOT NULL,
 * 			createdAt DATETIME NULL,
 * 			updatedAt DATETIME NULL,
 * 			url VARCHAR(255) NULL,
 * 			description VARCHAR(500) NULL,
 */
@Getter
@Setter
@Entity(name = "products")
@Table(name = "products")
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
public class Product extends BaseEntity<Long> {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    /**
     * the type of product
     */
    @Column(name = "type", nullable = false)
    private Integer type;

    /**
     * The title of product to be displayed
     */
    @Column(name = "title", nullable = false)
    private String title = "";

    /**
     * Product URL.
     */
    @Column(name = "url", nullable = false)
    private String url = "";

    /**
     * Product description.
     */
    @Column(name = "description")
    private String description = "";

    /**
     * The created time
     */
    @Column(name = "createdAt", nullable = false)
    private Date createdAt;

    /**
     * The updated time
     */
    @Column(name = "updatedAt", nullable = false)
    private Date updatedAt;

//    @ManyToMany(mappedBy = "categories")
    @ManyToMany
    private Set<Category> categories = new HashSet<>();

    /**
     * Add product to current category
     *
     * @param category category to be added to product
     */
    public void addCategory(final Category category) {
        if (category != null) {
            this.categories.add(category);
        }
    }

    /**
     * Add list of categories to product.
     *
     * @param categories the list of categories, which will be added to product
     */
    public void addCategories(final Collection<Category> categories) {
        if (isNotEmpty(categories)) {
            this.categories.addAll(categories);
        }
    }

    /**
     * Delete category from product
     */
    public void removeCategory(final Category category) {
        if (category != null) {
            this.categories.remove(category);
        }
    }

    /**
     * Remove the list of categories from product
     */
    public void removeCategories(final Collection<Category> categories) {
        if (isNotEmpty(categories)) {
            this.categories.removeAll(categories);
        }
    }

    /**
     * Clear all categories from product
     */
    public void clearCategories() {

        this.categories.clear();
    }

    /**
     * Convert categories to new collection and return
     */
    public Collection<Category> getCategories() {
        if(isNotEmpty(this.categories)){
            return new ArrayList<>(this.categories);
        }
        else{
            return new ArrayList<>();
        }
    }

    /**
     * Set collection of categories to product
     */
    public void setCategories(final Collection<Category> categories) {
        clearCategories();
        addCategories(categories);
    }

    @Override
    public Long getEntityId() {
        return getId();
    }
}
