package com.sfl.task.cafe.domain.orderprocess;

/**
 * Possible Order status enumeration
 */
public enum OrderStatus {
    /**
     * Order Started
     */
    NEW(1),

    /**
     * Order completed
     */
    COMPLETED(2),

    /**
     * Order cancelled
     */
    CANCELLED(3);

    private final int numStatus;

    OrderStatus(int numStatus) {
        this.numStatus = numStatus;
    }

    public String getDescription() {
        return toString();
    }

    public int getNumStatus() {
        return numStatus;
    }
}
