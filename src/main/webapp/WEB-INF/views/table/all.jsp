<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <meta name="title" content="Tables || Cafe Management">
        <title>Tables || Cafe Management</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="../other/waiter_navbar.jsp"/>
    <div class="container-fluid">
        <section id="tables">
            <div class="row admin-page">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                    <div class="row section-name text-shadow">
                        <b>
                            <span class="color-brown">Tables</span>
                            <c:if test="${fn:length(tables) eq 0}"><span class="color-red"> - Listy is empty!</span></c:if>
                        </b>
                    </div>
                </div>
                <c:if test="${fn:length(tables) gt 0}">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1 full-cart">
                        <table class="table">
                            <tr>
                                <th>Number</th>
                                <th>Capacity</th>
                                <th>Place</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </tr>
                            <c:forEach items="${tables}" var="table">
                                <tr>
                                    <td>${table.number}</td>
                                    <td>${table.capacity}</td>
                                    <td>${table.place}</td>
                                    <td>
                                        <c:choose>
                                            <c:when test="${table.status eq status_available}">
                                                <b><span class="color-green">${table.status}</span></b>
                                            </c:when>
                                            <c:when test="${table.status eq status_unavailable}">
                                                <span class="color-red">${table.status}</span>
                                            </c:when>
                                            <c:otherwise>
                                                <span class="color-brown">${table.status}</span>
                                            </c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>
                                        <a href="<c:url value="/waiter/table/view/${table.id}"/>"
                                           title="View ${table.number} table details">
                                            <button class="btn btn-info btn-mg" type="submit">View</button>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </c:if>
            </div>
        </section>
    </div>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    </body>
    </html>
</html>
