<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <meta name="title" content="Order ${order.orderCode} || Cafe Manager">
        <title>Order ${order.orderCode} || Cafe Manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="../../other/manager_navbar.jsp"/>
    <div class="container-fluid">
        <section id="order">
            <div class="row admin-page">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                    <div class="row section-name text-shadow">
                        <b><span class="color-brown">Order </span><span class="color-green">${order.orderCode}</span></b>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1 full-cart">
                    <table class="table">
                        <tr>
                            <th>Order Code:</th>
                            <td>${order.orderCode}</td>
                        </tr>
                        <tr>
                            <th>Status:</th>
                            <td>
                                <c:choose>
                                    <c:when test="${order.status eq status_new}">
                                        <span class="color-green"><b>${order.status}</b></span>
                                    </c:when>
                                    <c:otherwise>${order.status}</c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>Creation Date:</th>
                            <td>${order.createdAt}</td>
                        </tr>
                        <tr>
                            <th>Waiter:</th>
                            <td>
                                <c:choose>
                                    <c:when test="${water ne null}">
                                        <c:choose>
                                            <c:when test="${water.role eq maanger_role}">
                                                <span class="color-red">${water.role}</span>
                                            </c:when>
                                            <c:when test="${water.role eq waiter_role}">
                                                <span class="color-green">${water.role}</span>
                                            </c:when>
                                            <c:otherwise>${water.role}</c:otherwise>
                                        </c:choose>
                                        <a href="<c:url value="/manager/user/view/${water.id}"/>">
                                                ${water.firstName}
                                        </a>
                                    </c:when>
                                    <c:otherwise>-</c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>Details:</th>
                            <td>
                                <c:choose>
                                    <c:when test="${not empty order.description}">
                                        ${order.description}
                                    </c:when>
                                    <c:otherwise>-</c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>Order Items:</th>
                            <td>
                                <c:choose>
                                    <c:when test="${fn:length(orderItems) gt 0}">
                                        <c:forEach items="${orderItems}" var="item">
                                            <a href="<c:url value="/product/${item.product.url}"/>"
                                               title="Move to product ${item.product.title}">
                                                    ${item.product.title}
                                            </a>, № ${item.product.id},
                                            <br>${item.stockCode} x ${item.product.price} грн
                                            <br>--------------<br>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>The List Of Items Empty!</c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                        <tr>
                            <th>Total Price/th>
                            <td><b>${order.paidTotal}</b> грн</td>
                        </tr>
                        <tr>
                            <th></th>
                            <td>
                                <a href="<c:url value="/manager/order/edit/${order.id}"/>"
                                   title="Edit ${order.number} Order ">
                                    <button class="btn btn-success" type="submit">Edit</button>
                                </a>
                                <a href="<c:url value="/manager/order/delete/${order.id}"/>"
                                   title="Delete ${order.number} Order">
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </a>
                                <a href="<c:url value="/manager/order/all"/>" title="Back to all Orders">
                                    <button class="btn btn-info" type="submit">Back</button>
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </section>
    </div>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.maskedinput.min.js"/>"></script>
    </body>
    </html>
</html>
