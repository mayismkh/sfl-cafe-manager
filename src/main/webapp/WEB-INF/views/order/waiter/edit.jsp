<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <meta name="title" content="Edit ${order.orderCode} Order || Cafe Manager">
        <title>Edit ${order.orderCode} Order || Cafe Manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
        <%-- NAVBAR --%>
    <jsp:include page="../../other/waiter_navbar.jsp"/>
        <%-- Edit order --%>
    <div class="container-fluid">
        <section id="order">
            <div class="row manager-page">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                    <div class="row section-name text-shadow">
                        <b>
                            <span class="color-brown">Edit Order </span>
                            <span class="color-green">${order.orderCode}</span>
                        </b>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1 full-cart">
                    <form action="<c:url value="/waiter/order/update"/>" method="post">
                        <input type="hidden" name="id" value="${order.id}">
                        <input type="hidden" name="auth_user" value="${auth_user.id}">
                        <table class="table">
                            <tr>
                                <th>Status:</th>
                                <td>
                                    <select class="input-order" name="status" title="Order Status">
                                        <option value="${order.status}">${order.status}</option>
                                        <c:forEach items="${statuses}" var="status">
                                            <c:if test="${status ne order.status}">
                                                <option value="${status}">${status}</option>
                                            </c:if>
                                        </c:forEach>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <th>Created Date:</th>
                                <td>${order.createdAt}</td>
                            </tr>
                            <tr>
                                <th>Comments:</th>
                                <td>
                                <textarea class="input-order textarea" name="description"
                                          placeholder=" Comments" maxlength="250">${order.description}</textarea>
                                </td>
                            </tr>
                            <tr>
                                <th>Order Items:</th>
                                <td>
                                    <c:choose>
                                        <c:when test="${fn:length(orderItes) gt 0}">
                                            <c:forEach items="${orderItes}" var="item">
                                                <a href="<c:url value="/product/${item.product.url}"/>"
                                                   title="Move to ${item.product.title} Product">
                                                        ${item.product.title}
                                                </a>, № ${item.product.id},
                                                <br>${item.quantity} x ${item.price} $
                                                <br>--------------<br>
                                            </c:forEach>
                                        </c:when>
                                        <c:otherwise>The List of Items Empty!</c:otherwise>
                                    </c:choose>
                                </td>
                            </tr>
                            <tr>
                                <th>Total Price:</th>
                                <td>${order.paidTotal} $</td>
                            </tr>
                            <tr>
                                <th></th>
                                <td>
                                    <button class="btn btn-success" type="submit"
                                            title="Update Order details">Save</button>
                                    <button class="btn btn-info" type="reset"
                                            title="Reset edited data">Reset</button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </section>
    </div>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    </body>
    </html>
</html>
