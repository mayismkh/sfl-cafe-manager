<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <meta name="title" content="Personal || Cafe Manager">
        <title>Personal || Cafe Manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="../../other/waiter_navbar.jsp"/>
    <div class="container-fluid">
        <section id="persons">
            <div class="row admin-page">
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                    <div class="row section-name text-shadow">
                        <b>
                            <span class="color-brown">Personal</span>
                            <c:if test="${fn:length(users) eq 0}">
                                <span class="color-red"> - The list is empty!</span>
                                <br>
                                <a href="<c:url value="/manager/user/add"/>" title="Add new user">
                                    <button class="btn btn-success" type="submit">Add</button>
                                </a>
                            </c:if>
                        </b>
                    </div>
                </div>
                <c:if test="${fn:length(users) gt 0}">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1 full-cart">
                        <table class="table">
                            <tr>
                                <th>Role</th>
                                <th>Name</th>
                                <td class="hidden-xs"><b>Phone</b>
                                <td>
                                    <b>Actions</b>
                                    <a href="<c:url value="/manager/user/add"/>" title="Add new user">
                                        <button class="btn btn-success" type="submit">Add</button>
                                    </a>
                                    <a href="<c:url value="/manager/user/delete_all"/>" title="Delete all waiter">
                                        <button class="btn btn-danger" type="submit">Delete All</button>
                                    </a>
                                </td>
                            </tr>
                            <c:forEach items="${users}" var="user">
                                <tr>
                                    <td>
                                        <c:choose>
                                            <c:when test="${user.role eq manager_role}">
                                                <b><span class="color-red">${user.role}</span></b>
                                            </c:when>
                                            <c:when test="${user.role eq waiter_role}">
                                                <span class="color-green">${user.role}</span>
                                            </c:when>
                                            <c:otherwise>${user.role}</c:otherwise>
                                        </c:choose>
                                    </td>
                                    <td>${user.name}</td>
                                    <td class="hidden-xs">${user.phone}</td>
                                    <td>
                                        <a href="<c:url value="/manager/user/view/${user.id}"/>"
                                           title="View ${user.name} Detail">
                                            <button class="btn btn-info btn-mg" type="submit">View</button>
                                        </a>
                                        <a href="<c:url value="/manager/user/edit/${user.id}"/>"
                                           title="Edit ${user.name} Details">
                                            <button class="btn btn-success btn-mg" type="submit">Edit</button>
                                        </a>
                                        <a href="<c:url value="/manager/user/delete/${user.id}"/>"
                                           title="Delete ${user.name}">
                                            <button class="btn btn-danger btn-mg" type="submit">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </c:if>
            </div>
        </section>
    </div>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.maskedinput.min.js"/>"></script>
    </body>
    </html>
</html>

