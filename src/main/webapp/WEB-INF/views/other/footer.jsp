<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div class="container-fluid">
    <footer>
        <div class="row footer text-shadow text-center">
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                <h3>
                    <span class="footer-text-label color-green">Cafe</span>
                    <span class="footer-text-label color-brown">Manager</span>
                </h3>
                <h3>
                    <img src="<c:url value="/resources/img/favicon.png"/>" alt="Cafe Manager">
                </h3>
                <h3>
                    <span class="footer-text-label color-green">The Best </span>
                    <span class="footer-text-label color-brown">Cafe</span>
                </h3>
                <h5>
                    <a href="http://sfl.task.cafe.com" title="The Best Cafe">
                        sfl.task.cafe.com
                    </a>
                    &nbsp;Aramayis Mkhitaryan &copy;&nbsp;2021&nbsp;
                </h5>
            </div>
        </div>
    </footer>
</div>

