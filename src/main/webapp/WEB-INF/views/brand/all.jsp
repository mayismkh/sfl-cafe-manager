<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="ebg">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex,nofollow">
        <meta name="title" content="Brands || Cafe Manager">
        <title>Brands || Cafe Manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="../other/manager_navbar.jsp"/>
    <div class="container-fluid">
        <section id="brands">
            <div class="row maanger-page">
                <c:set var="brands_length" value="${fn:length(brands)}"/>
                <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                    <div class="row section-name text-shadow">
                        <b>
                            <span class="color-brown">Brands</span>
                            <c:if test="${brands_length eq 0}">
                                <span class="color-red"> - The list is empty!</span><br>
                                <a href="<c:url value="/manager/brand/add"/>" title="Add new brand">
                                    <button class="btn btn-success" type="submit">Add</button>
                                </a>
                            </c:if>
                        </b>
                    </div>
                </div>
                <c:if test="${brand_length gt 0}">
                    <div class="col-xs-12 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1 full-cart">
                        <table class="table">
                            <tr>
                                <th>Title</th>
                                <td class="hidden-xs"><b>Title</b></td>
                                <th>Url</th>
                                <td class="hidden-xs"><b>URL</b></td>
                                <th>Description</th>
                                <td class="hidden-xs"><b>Description</b></td>
                                <th>
                                    Actions
                                    <a href="<c:url value="/manager/brand/add"/>" title="Add New Brand">
                                        <button class="btn btn-success" type="submit">Add</button>
                                    </a>
                                    <a href="<c:url value="/manager/brand/delete_all"/>"
                                       title="Delete All Brands">
                                        <button class="btn btn-danger" type="submit">Delete All</button>
                                    </a>
                                </th>
                            </tr>
                            <c:forEach items="${brands}" var="brand">
                                <tr>
                                    <td>
                                        <a href="<c:url value="/brand/${brand.url}"/>"
                                           title=" Move to ${brand.title} brand">${brand.title}</a>
                                    </td>
                                    <td class="hidden-xs">${brand.url}</td>
                                    <td>
                                        <a href="<c:url value="/manager/brand/view/${brand.id}"/>"
                                           title="View ${brand.title} brand Details">
                                            <button class="btn btn-info" type="submit">View</button>
                                        </a>
                                        <a href="<c:url value="/manager/brand/edit/${brand.id}"/>"
                                           title="Edit ${brand.title} Brand">
                                            <button class="btn btn-success" type="submit">Edit</button>
                                        </a>
                                        <a href="<c:url value="/manager/brand/delete/${brand.id}"/>"
                                           title="Delete ${brand.title} Brand">
                                            <button class="btn btn-danger" type="submit">Delete</button>
                                        </a>
                                    </td>
                                </tr>
                            </c:forEach>
                        </table>
                    </div>
                </c:if>
            </div>
        </section>
    </div>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.maskedinput.min.js"/>"></script>
    </body>
    </html>
</html>

