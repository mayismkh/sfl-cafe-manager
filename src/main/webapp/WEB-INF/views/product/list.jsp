<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:if test="${fn:length(items) gt 0}">
    <c:forEach items="${items}" var="item">
        <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
            <div class="item">
                <a href="<c:url value="/product/${item.product.url}"/>" title="Move to ${item.product.title}">
                    <img src="<c:url value="/resources/img/${item.product.url}"/>"
                         alt="<c:out value="${item.product.title}"/>" class="img-thumbnail blink"
                         width="185px" height="185px">
                    <div class="text-shadow">
                    <c:out value="${item.product.title}"/>
                    </div>
                    <p class="price-top">
                        <fmt:formatNumber type="number" value="${item.price}"/> грн
                    </p>
                </a>
            </div>
        </div>
    </c:forEach>
</c:if>

