<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index,follow">
        <meta name="description" content="${item.product.description}"/>
        <meta name="keywords" content="${item.product.title}"/>
        <meta name="title" content="${item.product.title} || Cafe Manager">
        <title>${item.product.title} || Cafe Manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <jsp:include page="../other/client_navbar.jsp"/>
    <div class="container-fluid">
        <section id="one-product">
            <div class="row one-product">
                <div class="col-xs-7 col-xs-offset-1 col-sm-7 col-sm-offset-1 col-md-7 col-md-offset-1 col-lg-7 col-lg-offset-1">
                    <div class="col-xs-6 col-xs-offset-2 col-sm-6 col-sm-offset-2 col-md-6 col-md-offset-2 col-lg-6 col-lg-offset-2 col-xl-6 col-xl-offset-2">
                        <h3 class="text-shadow">
                            <b><c:out value="${item.product.title}"/></b>
                        </h3>
                        <h5>
                            Stock Code: <c:out value="${item.stockCode}"/>
                        </h5>
                        <h3>
                            <span class="price-product">
                                <fmt:formatNumber type="number" value="${item.price}"/> $
                            </span>
                        </h3>
                    </div>
                    <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                        <p><b>Brand:</b></p>
                        <p>${item.brand.title}</p>
                        <c:if test="${not empty item.product.description}">
                            <br>
                            <p><b>Product Description:</b></p>
                            <p><c:out value="${item.product.description}"/></p>
                        </c:if>
                        <br>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <jsp:include page="../other/footer.jsp"/>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/main.js"/>"></script>
    </body>
    </html>
</html>

