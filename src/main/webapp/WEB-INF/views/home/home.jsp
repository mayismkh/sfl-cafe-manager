<%@ page contentType="text/html;charset=UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<html removeIntertagSpaces="true">
    <!DOCTYPE HTML>
    <html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="index,follow">
        <meta name="google-site-verification" content="qiZQeYKdNTO5NVZQisl_gpnbTUCB89tSrwzSo99-fNo"/>
        <meta name="description"
              content="Cafe Manager - test task for SFL company."/>
        <meta name="keywords"
              content="cafe manager, sfl, test, task"/>
        <meta name="title" content="Cafe Manager || The best cafe manager">
        <title>Cafe Manager || The best cafe manager</title>
        <link rel="shortcut icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link rel="icon" href="<c:url value="/resources/img/favicon.ico"/>" type="image/x-icon">
        <link href="<c:url value="/resources/css/bootstrap.min.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/animate.css"/>" rel="stylesheet">
        <link href="<c:url value="/resources/css/style.min.css"/>" rel="stylesheet">
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body>
    <%@include file="navbar.jsp" %>
    <div class="container-fluid">
        <section id="main">
            <div class="row main text-shadow">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <span class="main-text-label color-green">Cafe</span>
                    <span class="main-text-label color-brown">Manager</span>
                    <span class="main-text-label color-green">The Best Cafe </span>
                    <span class="main-text-label color-brown"> Manager</span>
                </div>
            </div>
        </section>
    </div>
    <div class="container-fluid">
        <section id="categories">
            <div class="row categories">
                <c:set var="categories_length" value="${fn:length(categories)}"/>
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <h3 class="intro-text text-shadow">
                        <span class="home-block-name color-green">Product</span>
                        <span class="home-block-name color-brown"> Categories</span>
                        <c:if test="${categories_length eq 0}">
                            <span class="color-red"> - Empty List!</span>
                        </c:if>
                    </h3>
                </div>
                <c:if test="${categories_length gt 0}">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
                        <c:forEach items="${categories}" var="category">
                            <div class="col-xs-6 col-sm-6 col-md-6 col-lg-3 col-xl-3">
                                <div class="category">
                                    <a href="<c:url value="/category/${category.url}"/>"
                                       title="Move to category <c:out value="${category.title}"/>">
                                        <img src="<c:url value="/resources/img/${category.Url}"/>"
                                             class="img-thumbnail blink" width="150px" height="150px"
                                             alt="<c:out value="${category.title}"/>">
                                        <div class="text-shadow">
                                            <c:out value="${category.title}"/>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        </c:forEach>
                        <div class="col-xs-10 col-xs-offset-1 col-sm-10 col-sm-offset-1 col-md-10 col-md-offset-1 col-lg-10 col-lg-offset-1 col-xl-10 col-xl-offset-1">
                            <h4 class="text-all-products text-shadow">
                                <a href="<c:url value="/brand/all"/>" title="Move to All Products">
                                    All Products
                                </a>
                            </h4>
                        </div>
                    </div>
                </c:if>
            </div>
        </section>
    </div>
    <jsp:include page="product/list"/>
    <jsp:include page="other/footer"/>
    <script src="<c:url value="/resources/js/jquery-1.11.1.min.js"/>"></script>
    <script src="<c:url value="/resources/js/jquery.appear.js"/>"></script>
    <script src="<c:url value="/resources/js/bootstrap.min.js"/>"></script>
    <script src="<c:url value="/resources/js/main.js"/>"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    </body>
    </html>
</html>
