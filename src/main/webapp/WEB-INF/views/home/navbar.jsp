<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<div>
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                        aria-expanded="false" aria-controls="navbar">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <div class="navbar-brand text-shadow">
                    <a href="<c:url value="/home"/>">
                        <span class="nav-text-label color-green">Cafe</span>
                        <span class="nav-text-label color-brown">Manager</span>
                        <img src="<c:url value="/resources/img/favicon.png"/>" class="nav-label" alt="CafeManager">
                    </a>
                </div>
            </div>
            <div id="navbar" class="navbar-collapse collapse">
                <div id="menu">
                    <ul class="nav navbar-nav">
                        <li id="nav-main"><a href="#main">Main</a></li>
                        <li id="nav-categories"><a href="#categories">Categories</a></li>
                        <li id="nav-products"><a href="#products">Products</a></li>
                        <li id="nav-orders" class="hidden-sm"><a href="#delivery">Orders</a></li>
                        <li id="nav-transactions" class="hidden-sm"><a href="#payments">Transactions</a></li>
                        <li id="nav-users" class="hidden-sm"><a href="#contacts">Users</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
</div>
