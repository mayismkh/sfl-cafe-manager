/*********************************************************************************************************************
CreatedBy:     Aramayis Mkhitaryan
Project:		Cofe Managment
Description:    Create required tables
**********************************************************************************************************************/
SET ANSI_PADDING,ANSI_WARNINGS,CONCAT_NULL_YIELDS_NULL,ARITHABORT,QUOTED_IDENTIFIER,ANSI_NULLS ON

SET NUMERIC_ROUNDABORT OFF

SET NOCOUNT ON

BEGIN TRY
    BEGIN TRANSACTION

------------ users Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.users') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.users
        (
		    id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
			roleId  TINYINT NOT NULL,
			firstName VARCHAR(50) NULL,
			lastName VARCHAR(50) NULL,
			userName VARCHAR(50) NULL,
			mobile VARCHAR(50) NULL,
			email VARCHAR(50) NULL,
			passwordHash VARCHAR(500) NOT NULL,
			registeredAt DATETIME NOT NULL,
			lastLogin DATETIME NULL,

			CONSTRAINT PKC_users PRIMARY KEY CLUSTERED (id),


	        CONSTRAINT UC_users_userName
                UNIQUE NONCLUSTERED (userName),

	        CONSTRAINT UC_users_email
                UNIQUE NONCLUSTERED (email),
        ) 

		CREATE INDEX IDX_BY_USERNAME ON dbo.users(userName)
		CREATE INDEX IDX_BY_EMAIL ON dbo.users(email)

        -- table description
        EXEC sys.sp_addextendedproperty @name='MS_Description', 
            @value='The users collection', @level0name='dbo',
            @level0type='SCHEMA', @level1type='TABLE', @level1name='users'


    END

------------ brands Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.brands') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.brands
        (
		    id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
			title  VARCHAR(255) NOT NULL,
			url varchar(255) NOT NULL,
			description varchar(500) NOT NULL

			CONSTRAINT PKC_brands PRIMARY KEY CLUSTERED (id),
        ) 

        -- table description
        EXEC sys.sp_addextendedproperty @name='MS_Description', 
            @value='The brands collection', @level0name='dbo',
            @level0type='SCHEMA', @level1type='TABLE', @level1name='brands'


	END

------------ Categories Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.categories') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.categories
        (
		    id INT IDENTITY(1, 1) NOT FOR REPLICATION NOT NULL,
			parentCategoryId  INT NULL,
			title varchar(255) NOT NULL,
			url varchar(255) NOT NULL,
			description varchar(500) NOT NULL

			CONSTRAINT PKC_categories PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_categories_parentCategoryId_categories_Id
				FOREIGN KEY (parentCategoryId)
				REFERENCES dbo.categories(id),
        ) 

		CREATE INDEX IDX_BY_TITLE ON dbo.categories (title)
		CREATE INDEX IDX_BY_URL ON dbo.categories (url)

        -- table description
        EXEC sys.sp_addextendedproperty @name='MS_Description', 
            @value='The categories collection', @level0name='dbo',
            @level0type='SCHEMA', @level1type='TABLE', @level1name='categories'

        -- column descriptions
        EXEC sys.sp_addextendedproperty @name='MS_Description', 
            @value='Primary key', @level0type='SCHEMA', @level0name='dbo',
			@level1type='TABLE', @level1name='categories', @level2type='COLUMN', @level2name='id'

    END

------------ products Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.products') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.products
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			type SMALLINT NOT NULL DEFAULT 0,
			title VARCHAR(100) NOT NULL,
			createdAt DATETIME NULL,
			updatedAt DATETIME NULL,
			url VARCHAR(255) NULL,
			description VARCHAR(500) NULL,

			CONSTRAINT PKC_products PRIMARY KEY CLUSTERED (id),
        ) 

    END
------------ product_category Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.productCategory') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.productCategory
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			productId INT NOT NULL,
			categoryId INT NOT NULL,

			CONSTRAINT PKC_product_category PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_productCategory_categoryId_categories_id
				FOREIGN KEY (categoryId)
				REFERENCES dbo.categories(id),

			CONSTRAINT FK_productCategory_productId_products_id
				FOREIGN KEY (productId)
				REFERENCES dbo.products (id),

	        CONSTRAINT UC_productCategory_productId_categoryId
                UNIQUE NONCLUSTERED (productId, categoryId)
		) 

    END
------------ cafeTables Table --------------------------------------------------------------------------------------------------------

    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.cafeTables') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.cafeTables
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			number INT NOT NULL,
			waiterId INT NULL,
			status TINYINT NOT NULL DEFAULT 0,
			capacity INT NOT NULL,
			place VARCHAR(50) NULL

			CONSTRAINT PKC_cafeTables PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_cafe_tables_waiterId_users_id
				FOREIGN KEY (waiterId)
				REFERENCES dbo.users (id),
		) 

    END
------------ orders Table --------------------------------------------------------------------------------------------------------
    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.orders') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.orders
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			userId INT NOT NULL,
			orderCode VARCHAR(100) NOT NULL,
			cafeTableId INT NOT NULL,
			status TINYINT NOT NULL,
			total DECIMAL NOT NULL DEFAULT 0,
			discount DECIMAL NOT NULL DEFAULT 0,
			ItemDiscount DECIMAL NOT NULL DEFAULT 0,
			paidTotal DECIMAL NOT NULL DEFAULT 0,
			createdAt DATETIME NOT NULL,
			updatedAt DATETIME NULL,

			CONSTRAINT PKC_orders PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_orders_userId_users_id
				FOREIGN KEY (userId)
				REFERENCES dbo.users (id),

			CONSTRAINT FK_orders_userId_cafeTables_id
				FOREIGN KEY (cafeTableId)
				REFERENCES dbo.cafeTables (id),
		) 


    END

------------ items Table --------------------------------------------------------------------------------------------------------

    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.items') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.items
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			productId INT NOT NULL,
			brandId INT NOT NULL,
			supplierId INT NOT NULL,
			orderId INT NOT NULL,
			stockCode VARCHAR(100) NOT NULL,
			retailPrice DECIMAL NOT NULL DEFAULT 0,
			discount DECIMAL NOT NULL DEFAULT 0,
			price DECIMAL NOT NULL DEFAULT 0,
			quantity INT NOT NULL DEFAULT 0,
			sold INT NOT NULL DEFAULT 0,
			available INT NOT NULL DEFAULT 0,
			createdAt DATETIME NOT NULL,
			updatedAt DATETIME NULL DEFAULT NULL,

			CONSTRAINT PKC_items PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_items_supplierId_users_id
				FOREIGN KEY (supplierId)
				REFERENCES dbo.users(id),

			CONSTRAINT FK_items_productId_products_id
				FOREIGN KEY (productId)
				REFERENCES dbo.products (id),

			CONSTRAINT FK_items_brandId_brands_id
				FOREIGN KEY (brandId)
				REFERENCES dbo.brands (id),

			CONSTRAINT FK_items_orderId_orders_id
				FOREIGN KEY (orderId)
				REFERENCES dbo.orders(id),
		) 

		CREATE INDEX IDX_BY_SUPLIERID ON dbo.items (supplierId)
		CREATE INDEX IDX_BY_ORDERID ON dbo.items(orderId)
		CREATE INDEX IDX_BY_BRANDID ON dbo.items (brandId)
		CREATE INDEX IDX_BY_PRODUCTID ON dbo.items (productId)

    END

------------ orderItems Table --------------------------------------------------------------------------------------------------------

    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.orderItems') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.orderItems
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			productId INT NOT NULL,
			itemId INT NOT NULL,
			orderId INT NOT NULL,
			stockCode VARCHAR(100) NOT NULL,
			discount DECIMAL NOT NULL DEFAULT 0,
			price DECIMAL NOT NULL DEFAULT 0,
			quantity INT NOT NULL DEFAULT 0,
			createdAt DATETIME NOT NULL,
			updatedAt DATETIME NULL DEFAULT NULL,

			CONSTRAINT PKC_orderItems PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_orderItems_itemId_items_id
				FOREIGN KEY (itemId)
				REFERENCES dbo.items(id),

			CONSTRAINT FK_orderItems_productId_products_id
				FOREIGN KEY (productId)
				REFERENCES dbo.products (id),

			CONSTRAINT FK_orderItems_orderId_orders_id
				FOREIGN KEY (orderId)
				REFERENCES dbo.orders(id),
		) 

		CREATE INDEX IDX_BY_ITEMID ON dbo.orderItems (itemId)
		CREATE INDEX IDX_BY_ORDERID ON dbo.orderItems(orderId)
		CREATE INDEX IDX_BY_PRODUCTID ON dbo.orderItems (productId)

    END
------------ transactions  Table --------------------------------------------------------------------------------------------------------

    IF NOT EXISTS (SELECT * FROM sys.tables WHERE object_id = OBJECT_ID(N'dbo.transactions ') AND is_ms_shipped = 0)
    BEGIN
        -- table create
        CREATE TABLE dbo.transactions
        (
			id INT IDENTITY(1,1) NOT FOR REPLICATION NOT NULL,
			userId INT NOT NULL,
			orderId INT NOT NULL,
			status SMALLINT NOT NULL  DEFAULT 0,
			type SMALLINT NOT NULL  DEFAULT 0,
			mode SMALLINT NOT NULL  DEFAULT 0,
			createdAt DATETIME NOT NULL,
			updatedAt DATETIME NULL,

			CONSTRAINT PKC_transactions PRIMARY KEY CLUSTERED (id),

			CONSTRAINT FK_transactions_userId_users_id
				FOREIGN KEY (userId)
				REFERENCES dbo.users (id),

			CONSTRAINT FK_transactions_orderId_orders_id
				FOREIGN KEY (orderId)
				REFERENCES dbo.orders(id),
		) 

		CREATE INDEX IDX_BY_ORDERID ON dbo.transactions(orderId)
		CREATE INDEX IDX_BY_USERID ON dbo.transactions (userId)


    END

---------------------------------------------------------------------------------------------------------------------
    IF @@TRANCOUNT <> 0
        COMMIT TRANSACTION
END TRY
BEGIN CATCH
    DECLARE @errorNumber INT,
            @errorMessage NVARCHAR(2048),
            @errorSeverity INT,
            @errorState INT,
            @errorLine INT,
            @errorProcedure NVARCHAR(126)

    SELECT @errorNumber = ERROR_NUMBER(),
           @errorSeverity = ERROR_SEVERITY(),
           @errorState = ERROR_STATE(),
           @errorLine = ERROR_LINE(),
           @errorProcedure = ERROR_PROCEDURE(),
           @errorMessage = 'Msg - %i, Prc - %s, Ln - %i: ' + ERROR_MESSAGE()

    IF @@TRANCOUNT <> 0
        ROLLBACK TRANSACTION

    RAISERROR(@errorMessage, @errorSeverity, @errorState, @errorNumber, @errorProcedure, @errorLine)
END CATCH

GO
	--GRANT DELETE  ON [dbo].users  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].users  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].users  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].users  TO sfl_newcomer   
		
	--GRANT DELETE  ON [dbo].brands  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].brands  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].brands  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].brands  TO sfl_newcomer    

	--GRANT DELETE  ON [dbo].categories  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].categories  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].categories  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].categories  TO sfl_newcomer

	--GRANT DELETE  ON [dbo].products  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].products  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].products  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].products  TO sfl_newcomer
		
	--GRANT DELETE  ON [dbo].productCategory  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].productCategory  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].productCategory  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].productCategory  TO sfl_newcomer	
		
	--GRANT DELETE  ON [dbo].cafeTables  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].cafeTables  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].cafeTables  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].cafeTables  TO sfl_newcomer
		
	--GRANT DELETE  ON [dbo].orders  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].orders  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].orders  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].orders  TO sfl_newcomer
		
	--GRANT DELETE  ON [dbo].items  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].items  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].items  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].items  TO sfl_newcomer

	--GRANT DELETE  ON [dbo].orderItems  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].orderItems  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].orderItems  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].orderItems  TO sfl_newcomer

	--GRANT DELETE  ON [dbo].transactions  TO sfl_newcomer
	--GRANT SELECT  ON [dbo].transactions  TO sfl_newcomer
	--GRANT UPDATE  ON [dbo].transactions  TO sfl_newcomer
	--GRANT INSERT  ON [dbo].transactions  TO sfl_newcomer		

GO
--DROP TABLE dbo.productCategory
--DROP TABLE dbo.orderItems 
--DROP TABLE dbo.items 
--DROP TABLE dbo.transactions
--DROP TABLE dbo.categories 
--DROP TABLE dbo.products
--DROP TABLE dbo.brands
--DROP TABLE dbo.orders
--DROP TABLE dbo.cafeTables
--DROP TABLE dbo.users

GO